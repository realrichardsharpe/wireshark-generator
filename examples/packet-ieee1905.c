//argNum: 0 = xxx.proto
#include "config.h"
#include <epan/packet.h>

static int proto_ieee1905 = -1;
static int hf_ieee1905_al_mac_address_info_1905_al_mac_address_of_transmitting_device = -1;

static int hf_ieee1905_mac_address_info_mac_address_of_transmitting_interface = -1;

static int hf_local_interface_details_local_interface = -1;
static int hf_local_interface_details_media_type = -1;
static int hf_local_interface_details_media_specific_info_len = -1;
static int hf_local_interface_details_media_specific_information = -1;

static int hf_ieee1905_device_information_1905_al_mac_address_of_device = -1;
static int hf_ieee1905_device_information_local_interface_count = -1;

static int hf_channel_preference_flags_0_preference = -1;
static int hf_channel_preference_flags_0_reason = -1;

static int hf_channel_preference_detail_operating_class = -1;
static int hf_channel_preference_detail_channel_list_count = -1;
static int hf_channel_preference_detail_channel_list = -1;

static int hf_ieee1905_channel_preference_radio_unique_identifier = -1;
static int hf_ieee1905_channel_preference_operating_classes = -1;

static int hf_ieee1905_vendor_specific_vendor_specific_oui = -1;
static int hf_ieee1905_vendor_specific_vendor_specific_information = -1;

static int hf_steering_request_flags_0_request_mode = -1;
static int hf_steering_request_flags_0_btm_disassociation_imminent = -1;
static int hf_steering_request_flags_0_btm_abridged = -1;
static int hf_steering_request_flags_0_reserved = -1;

static int hf_steering_op_window_steering_opportunity_window = -1;

static int hf_target_bssid_info_target_bssid = -1;
static int hf_target_bssid_info_target_bssid_operating_class = -1;
static int hf_target_bssid_info_target_bssid_channel_number = -1;

static int hf_ieee1905_steering_request_source_bss_bssid = -1;
static int hf_ieee1905_steering_request_btm_disassociation_timer = -1;
static int hf_ieee1905_steering_request_sta_list_count = -1;
static int hf_ieee1905_steering_request_target_mac_address = -1;
static int hf_ieee1905_steering_request_target_bssid_list_count = -1;

static int hf_ieee1905_steering_btm_report_report_bssid = -1;
static int hf_ieee1905_steering_btm_report_reported_sta_mac_address = -1;
static int hf_ieee1905_steering_btm_report_btm_status_code = -1;
static int hf_ieee1905_steering_btm_report_target_bssid = -1;

static int hf_unknown_tlv_type_unknown_tlv_data = -1;

static int hf_tlv_header_type_tlvtype = -1;
static int hf_tlv_header_type_tlvlength = -1;


static int hf_ieee1905_cmdu_messageversion = -1;
static int hf_ieee1905_cmdu_reserved = -1;
static int hf_ieee1905_cmdu_messagetype = -1;
static int hf_ieee1905_cmdu_messageid = -1;
static int hf_ieee1905_cmdu_fragmentid = -1;
static int hf_ieee1905_cmdu_5_lastfragmentindicator = -1;
static int hf_ieee1905_cmdu_5_relayindicator = -1;
static int hf_ieee1905_cmdu_5_reserved = -1;

static gint ett_ieee1905_al_mac_address_info = -1;

static gint ett_ieee1905_mac_address_info = -1;

static gint ett_local_interface_details = -1;

static gint ett_ieee1905_device_information = -1;
static gint ett_local_interface_details_list = -1;

static gint ett_channel_preference_flags = -1;

static gint ett_channel_preference_detail = -1;
static gint ett_uint8_list = -1;
static gint ett_uint8 = -1;

static gint ett_ieee1905_channel_preference = -1;
static gint ett_channel_preference_detail_list = -1;

static gint ett_ieee1905_vendor_specific = -1;

static gint ett_steering_request_flags = -1;

static gint ett_steering_op_window = -1;

static gint ett_target_bssid_info = -1;

static gint ett_ieee1905_steering_request = -1;
static gint ett_ether_t_list = -1;
static gint ett_ether_t = -1;
static gint ett_target_bssid_info_list = -1;

static gint ett_ieee1905_steering_btm_report = -1;

static gint ett_unknown_tlv_type = -1;

static gint ett_tlv_header_type = -1;

static gint ett_protocol_tlv = -1;

static gint ett_ieee1905_cmdu = -1;
static gint ett_protocol_tlv_list = -1;

enum message_version_enum {
  VERSION_1 = 0x00,
};

static const range_string message_version_enum_rvals[] = {
  { 0x00, 0x00, "Version 1" },
  { 1, 255, "Reserved" }
};

enum last_fragment_enum {
  NOT_LAST_FRAG = 0x0,
  LAST_FRAG     = 0x1,
};

static const true_false_string last_fragment_enum_tfs = {
  "The last fragment",
  "Not the last fragment",
};

enum relay_indicator_enum {
  NOT_RELAYED = 0x0,
  RELAYED     = 0x1,
};

static const true_false_string relay_indicator_enum_tfs = {
  "Shall be relayed",
  "Not to be relayed",
};

enum message_type_enum {
  TOPOLOGY_DISCOVERY               = 0x0000,
  TOPOLOGY_NOTIFICATION            = 0x0001,
  TOPOLOGY_QUERY                   = 0x0002,
  TOPOLOGY_RESPONSE                = 0x0003,
  VENDOR_SPECIFIC                  = 0x0004,
  LINK_METRIC_QUERY                = 0x0005,
  LINK_METRIC_RESPONSE             = 0x0006,
  AP_AUTOCONFIGURATION_SEARCH      = 0x0007,
  AP_AUTOCONFIGURATION_RESPONSE    = 0x0008,
  AP_AUTOCONFIGURATION_WSC         = 0x0009,
  AP_AUTOCONFIGURATION_RENEW       = 0x000A,
  IEEE1905_PUSH_BUTTON_EVENT_NOTIF = 0x000B,
  IEEE1905_PUSH_BUTTON_JOIN_NOTIF  = 0x000C,
  HIGHER_LAYER_QUERY               = 0x000D,
  HIGHER_LAYER_RESPONSE            = 0x000E,
  INTERFACE_POWER_CHANGE_REQ       = 0x000F,
  INTERFACE_POWER_CHANGE_RSP       = 0x0010,
  GENERIC_PHY_QUERY                = 0x0011,
  GENERIC_PHY_RSP                  = 0x0012,
};

static const range_string message_type_enum_rvals[] = {
  { 0x0000, 0x0000, "Topology discovery" },
  { 0x0001, 0x0001, "Topology notification" },
  { 0x0002, 0x0002, "Topology query" },
  { 0x0003, 0x0003, "Topology response" },
  { 0x0004, 0x0004, "Vendor specific" },
  { 0x0005, 0x0005, "Link metric query" },
  { 0x0006, 0x0006, "Link metric response" },
  { 0x0007, 0x0007, "AP-autoconfiguration search" },
  { 0x0008, 0x0008, "AP-autoconfiguration response" },
  { 0x0009, 0x0009, "AP-autoconfiguration WSC" },
  { 0x000A, 0x000A, "AP-autoconfiguration renew" },
  { 0x000B, 0x000B, "1905 push button event notification" },
  { 0x000C, 0x000C, "1905 push button join notification" },
  { 0x000D, 0x000D, "Higher Layer query" },
  { 0x000E, 0x000E, "Higher Layer response" },
  { 0x000F, 0x000F, "Interface Power Change Request" },
  { 0x0010, 0x0010, "Interface Power Change Response" },
  { 0x0011, 0x0011, "Generic Phy query" },
  { 0x0012, 0x0012, "Generic Phy response" },
  { 19, 65535, "Reserved" }
};

enum media_type {
  FAST_ENET = 0x0000,
  GB_ENET   = 0x0001,
  WIFI_B    = 0x0100,
  WIFI_G    = 0x0101,
  WIFI_A5   = 0x0102,
  WIFI_A24  = 0x0103,
  WIFI_N5   = 0x0104,
  WIFI_N24  = 0x0105,
  WIFI_AD   = 0x0106,
  WIFI_AF   = 0x0107,
  IEEE1901W = 0x0200,
  IEEE1901F = 0x0201,
  MOCA11    = 0x0300,
};

static const range_string media_type_rvals[] = {
  { 0x0000, 0x0000, "IEEE 802.3u fast Ethernet" },
  { 0x0001, 0x0001, "IEEE 802.3ab gigabit Ethernet" },
  { 2, 255, "Reserved" },
  { 0x0100, 0x0100, "IEEE 802.11b (2.4 GHz)" },
  { 0x0101, 0x0101, "IEEE 802.11g (2.4 GHz)" },
  { 0x0102, 0x0102, "IEEE 802.11a (5GHz)" },
  { 0x0103, 0x0103, "IEEE 802.11a (2.4GHz)" },
  { 0x0104, 0x0104, "IEEE 802.11n (5GHz)" },
  { 0x0105, 0x0105, "IEEE 802.11n (2.4GHz)" },
  { 0x0106, 0x0106, "IEEE 802.11ad (60GHz)" },
  { 0x0107, 0x0107, "IEEE 802.11af (whitespace)" },
  { 264, 511, "Reserved" },
  { 0x0200, 0x0200, "IEEE 1901 wavelet" },
  { 0x0201, 0x0201, "IEEE 1901 FFT" },
  { 514, 767, "Reserved" },
  { 0x0300, 0x0300, "MoCA v1.1" },
  { 769, 65535, "Reserved" }
};

enum channel_preference_prefs {
  NON_OPERABLE    = 0x0,
  OPERABLE_PREF_1 = 0x1,
  OPERABLE_PREF_2 = 0x2,
};

static const range_string channel_preference_prefs_rvals[] = {
  { 0x0, 0x0, "Non-operable" },
  { 0x1, 0x1, "Operable with preferance score 1" },
  { 0x2, 0x2, "Operable with preference score 2" },
  { 3, 15, "Reserved" }
};

enum channel_preference_reason {
  UNSPECIFIED          = 0x0,
  PROXIMATE_INTERFERER = 0x1,
};

static const range_string channel_preference_reason_rvals[] = {
  { 0x0, 0x0, "Unspecified" },
  { 0x1, 0x1, "Proximate non-802.11 interferer in local environment" },
  { 2, 15, "Reserved" }
};

enum tlv_type_enum {
  IEEE1905_END_OF_MESSAGE_TLV               = 0,
  IEEE1905_AL_MAC_ADDRESS_TYPE_TLV          = 1,
  IEEE1905_MAC_ADDRESS_TYPE_TLV             = 2,
  IEEE1905_DEVICE_INFORMATION_TYPE_TLV      = 3,
  IEEE1905_DEVICE_BRIDGING_CAPABILITY_TLV   = 4,
  IEEE1905_NON1905_NEIGHBOR_DEVICE_LIST_TLV = 6,
  IEEE1905_1905_NEIGHBOR_DEVICE_LIST_TLV    = 7,
  IEEE1905_LINK_METRIC_QUERY_TLV            = 8,
  IEEE1905_TRANSMITTER_LINK_METRIC_TLV      = 9,
  IEEE1905_RECEIVER_LINK_METRIC_TLV         = 10,
  IEEE1905_VENDOR_SPECIFIC_TLV              = 11,
  IEEE1905_CHANNEL_PREFERENCE_TLV           = 0x8B,
  IEEE1905_STEERING_REQUEST_TLV             = 0x9B,
  IEEE1905_STEERING_BTM_REPORT_TLV          = 0x9C,
};

static const range_string tlv_type_enum_rvals[] = {
  { 0, 0, "End of message" },
  { 1, 1, "1905 AL MAC address type" },
  { 2, 2, "MAC address type" },
  { 3, 3, "Device information type" },
  { 4, 4, "Device bridging capability" },
  { 5, 5, "Reserved" },
  { 6, 6, "Non-1905 neighbor device list" },
  { 7, 7, "1905 neighbor device" },
  { 8, 8, "Link metric query" },
  { 9, 9, "1905 transmitter link metric" },
  { 10, 10, "1905 receiver link metric" },
  { 11, 11, "Vendor specific" },
  { 12, 138, "Reserved" },
  { 0x8B, 0x8B, "Channel Preference" },
  { 140, 154, "Reserved" },
  { 0x9B, 0x9B, "Steering Request" },
  { 0x9C, 0x9C, "Steering BTM Report" },
  { 157, 255, "Reserved" }
};

enum steering_request_mode {
  REQUEST_IS_STEERING_OPPORTUNITY = 0,
  REQUEST_IS_STEERING_MANDATE     = 1,
};

static const true_false_string steering_request_mode_tfs = {
  "Request is a steering mandate",
  "Request is a steering opportunity",
};

static int
dissect_ieee1905_al_mac_address_info(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName);

static int
dissect_ieee1905_mac_address_info(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName);

static int
dissect_local_interface_details(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName);

static int
dissect_ieee1905_device_information(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName);

static int
dissect_channel_preference_flags(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName);

static int
dissect_channel_preference_detail(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName);

static int
dissect_ieee1905_channel_preference(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName);

static int
dissect_ieee1905_vendor_specific(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName, guint16 tlv_header_tlvlength);

static int
dissect_steering_request_flags(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName, guint8 *steering_request_flags_request_mode);

static int
dissect_steering_op_window(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName);

static int
dissect_target_bssid_info(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName);

static int
dissect_ieee1905_steering_request(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName);

static int
dissect_ieee1905_steering_btm_report(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName, guint16 tlv_header_tlvlength);

static int
dissect_unknown_tlv_type(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName, guint16 tlv_header_tlvlength);

static int
dissect_tlv_header_type(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName, guint8 *tlv_header_tlvtype, guint16 *tlv_header_tlvlength);

static int
dissect_protocol_tlv(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName);

static int
dissect_ieee1905_cmdu(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, void *data);


static int
dissect_ieee1905_al_mac_address_info(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName)
{
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_ieee1905_al_mac_address_info, &str_item, dispName);
  proto_tree_add_item(sub_tree, hf_ieee1905_al_mac_address_info_1905_al_mac_address_of_transmitting_device, tvb, offset, 6, ENC_NA);
  offset += 6;
  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

static int
dissect_ieee1905_mac_address_info(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName)
{
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_ieee1905_mac_address_info, &str_item, dispName);
  proto_tree_add_item(sub_tree, hf_ieee1905_mac_address_info_mac_address_of_transmitting_interface, tvb, offset, 6, ENC_NA);
  offset += 6;
  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

static int
dissect_local_interface_details(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName)
{
  guint8 media_specific_info_len;
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_local_interface_details, &str_item, dispName);
  proto_tree_add_item(sub_tree, hf_local_interface_details_local_interface, tvb, offset, 6, ENC_NA);
  offset += 6;
  proto_tree_add_item(sub_tree, hf_local_interface_details_media_type, tvb, offset, 2, ENC_NA);
  offset += 2;
  media_specific_info_len = tvb_get_guint8(tvb, offset);
  proto_tree_add_item(sub_tree, hf_local_interface_details_media_specific_info_len, tvb, offset, 1, ENC_NA);
  offset += 1;
  proto_tree_add_item(sub_tree, hf_local_interface_details_media_specific_information, tvb, offset, media_specific_info_len, ENC_NA);
  offset += media_specific_info_len;

  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

static int
dissect_ieee1905_device_information(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName)
{
  guint8 local_interface_count;
  int i = 0;
  proto_tree *list_tree = NULL;
  proto_item *pi = NULL;
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_ieee1905_device_information, &str_item, dispName);
  proto_tree_add_item(sub_tree, hf_ieee1905_device_information_1905_al_mac_address_of_device, tvb, offset, 6, ENC_NA);
  offset += 6;
  local_interface_count = tvb_get_guint8(tvb, offset);
  proto_tree_add_item(sub_tree, hf_ieee1905_device_information_local_interface_count, tvb, offset, 1, ENC_NA);
  offset += 1;
  list_tree = proto_tree_add_subtree(sub_tree, tvb, offset, -1, ett_local_interface_details_list, &pi, "Local interface list list");

  for (i = 0; i < local_interface_count; i++) {
    proto_tree *list_item = NULL;
    proto_item *ppi = NULL;
    int loop_saved_offset = offset;

    list_item = proto_tree_add_subtree_format(list_tree, tvb, offset, -1, ett_local_interface_details, &ppi, "Local interface list item %d", i);
    offset = dissect_local_interface_details(tvb, pinfo, list_item, offset, "Local interface list");
    proto_item_set_len(ppi, offset - loop_saved_offset);
  }
  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

static int
dissect_channel_preference_flags(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName)
{
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_channel_preference_flags, &str_item, dispName);
  proto_tree_add_item(sub_tree, hf_channel_preference_flags_0_preference, tvb, offset, 1, ENC_NA);
  proto_tree_add_item(sub_tree, hf_channel_preference_flags_0_reason, tvb, offset, 1, ENC_NA);
  offset += 1;
  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

static int
dissect_channel_preference_detail(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName)
{
  guint8 channel_list_count;
  int i = 0;
  proto_tree *list_tree = NULL;
  proto_item *pi = NULL;
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_channel_preference_detail, &str_item, dispName);
  proto_tree_add_item(sub_tree, hf_channel_preference_detail_operating_class, tvb, offset, 1, ENC_NA);
  offset += 1;
  channel_list_count = tvb_get_guint8(tvb, offset);
  proto_tree_add_item(sub_tree, hf_channel_preference_detail_channel_list_count, tvb, offset, 1, ENC_NA);
  offset += 1;
  list_tree = proto_tree_add_subtree(sub_tree, tvb, offset, -1, ett_uint8_list, &pi, "Channel list list");

  for (i = 0; i < channel_list_count; i++) {
    proto_tree *list_item = NULL;
    proto_item *ppi = NULL;
    int loop_saved_offset = offset;

    list_item = proto_tree_add_subtree_format(list_tree, tvb, offset, -1, ett_uint8, &ppi, "Channel list item %d", i);
    proto_tree_add_item(list_item, hf_channel_preference_detail_channel_list, tvb, offset, 1, ENC_NA);
    offset += 1;
    proto_item_set_len(ppi, offset - loop_saved_offset);
  }
  offset = dissect_channel_preference_flags(tvb, pinfo, sub_tree, offset, "Flags");
  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

static int
dissect_ieee1905_channel_preference(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName)
{
  guint8 operating_classes;
  int i = 0;
  proto_tree *list_tree = NULL;
  proto_item *pi = NULL;
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_ieee1905_channel_preference, &str_item, dispName);
  proto_tree_add_item(sub_tree, hf_ieee1905_channel_preference_radio_unique_identifier, tvb, offset, 6, ENC_NA);
  offset += 6;
  operating_classes = tvb_get_guint8(tvb, offset);
  proto_tree_add_item(sub_tree, hf_ieee1905_channel_preference_operating_classes, tvb, offset, 1, ENC_NA);
  offset += 1;
  list_tree = proto_tree_add_subtree(sub_tree, tvb, offset, -1, ett_channel_preference_detail_list, &pi, "Operating class list list");

  for (i = 0; i < operating_classes; i++) {
    proto_tree *list_item = NULL;
    proto_item *ppi = NULL;
    int loop_saved_offset = offset;

    list_item = proto_tree_add_subtree_format(list_tree, tvb, offset, -1, ett_channel_preference_detail, &ppi, "Operating class list item %d", i);
    offset = dissect_channel_preference_detail(tvb, pinfo, list_item, offset, "Operating class list");
    proto_item_set_len(ppi, offset - loop_saved_offset);
  }
  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

static int
dissect_ieee1905_vendor_specific(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName, guint16 tlv_header_tlvlength)
{
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_ieee1905_vendor_specific, &str_item, dispName);
  proto_tree_add_item(sub_tree, hf_ieee1905_vendor_specific_vendor_specific_oui, tvb, offset, 3, ENC_NA);
  offset += 3;
  proto_tree_add_item(sub_tree, hf_ieee1905_vendor_specific_vendor_specific_information, tvb, offset, tlv_header_tlvlength - 3, ENC_NA);
  offset += tlv_header_tlvlength - 3;

  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

static int
dissect_steering_request_flags(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName, guint8 *steering_request_flags_request_mode)
{
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_steering_request_flags, &str_item, dispName);
  *steering_request_flags_request_mode = (tvb_get_guint8(tvb, offset) >> 7)& ((1 << 1) - 1);
  proto_tree_add_item(sub_tree, hf_steering_request_flags_0_request_mode, tvb, offset, 1, ENC_NA);
  proto_tree_add_item(sub_tree, hf_steering_request_flags_0_btm_disassociation_imminent, tvb, offset, 1, ENC_NA);
  proto_tree_add_item(sub_tree, hf_steering_request_flags_0_btm_abridged, tvb, offset, 1, ENC_NA);
  proto_tree_add_item(sub_tree, hf_steering_request_flags_0_reserved, tvb, offset, 1, ENC_NA);
  offset += 1;
  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

static int
dissect_steering_op_window(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName)
{
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_steering_op_window, &str_item, dispName);
  proto_tree_add_item(sub_tree, hf_steering_op_window_steering_opportunity_window, tvb, offset, 2, ENC_BIG_ENDIAN);
  offset += 2;
  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

static int
dissect_target_bssid_info(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName)
{
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_target_bssid_info, &str_item, dispName);
  proto_tree_add_item(sub_tree, hf_target_bssid_info_target_bssid, tvb, offset, 6, ENC_NA);
  offset += 6;
  proto_tree_add_item(sub_tree, hf_target_bssid_info_target_bssid_operating_class, tvb, offset, 1, ENC_NA);
  offset += 1;
  proto_tree_add_item(sub_tree, hf_target_bssid_info_target_bssid_channel_number, tvb, offset, 1, ENC_NA);
  offset += 1;
  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

static int
dissect_ieee1905_steering_request(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName)
{
  guint8 steering_request_flags_request_mode;
  guint8 sta_list_count;
  guint8 target_bssid_list_count;
  int i = 0;
  proto_tree *list_tree = NULL;
  proto_item *pi = NULL;
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_ieee1905_steering_request, &str_item, dispName);
  proto_tree_add_item(sub_tree, hf_ieee1905_steering_request_source_bss_bssid, tvb, offset, 6, ENC_NA);
  offset += 6;
  offset = dissect_steering_request_flags(tvb, pinfo, sub_tree, offset, "Steering request flags", &steering_request_flags_request_mode);
  switch (steering_request_flags_request_mode) {
  case REQUEST_IS_STEERING_OPPORTUNITY:
    break;

  case REQUEST_IS_STEERING_MANDATE:
    offset = dissect_steering_op_window(tvb, pinfo, sub_tree, offset, "steering_op_window");
    break;

  }
  proto_tree_add_item(sub_tree, hf_ieee1905_steering_request_btm_disassociation_timer, tvb, offset, 2, ENC_BIG_ENDIAN);
  offset += 2;
  sta_list_count = tvb_get_guint8(tvb, offset);
  proto_tree_add_item(sub_tree, hf_ieee1905_steering_request_sta_list_count, tvb, offset, 1, ENC_NA);
  offset += 1;
  list_tree = proto_tree_add_subtree(sub_tree, tvb, offset, -1, ett_ether_t_list, &pi, "Target MAC address list");

  for (i = 0; i < sta_list_count; i++) {
    proto_tree *list_item = NULL;
    proto_item *ppi = NULL;
    int loop_saved_offset = offset;

    list_item = proto_tree_add_subtree_format(list_tree, tvb, offset, -1, ett_ether_t, &ppi, "Target MAC address item %d", i);
    proto_tree_add_item(list_item, hf_ieee1905_steering_request_target_mac_address, tvb, offset, 6, ENC_NA);
    offset += 6;
    proto_item_set_len(ppi, offset - loop_saved_offset);
  }
  target_bssid_list_count = tvb_get_guint8(tvb, offset);
  proto_tree_add_item(sub_tree, hf_ieee1905_steering_request_target_bssid_list_count, tvb, offset, 1, ENC_NA);
  offset += 1;
  list_tree = proto_tree_add_subtree(sub_tree, tvb, offset, -1, ett_target_bssid_info_list, &pi, "Target BSSID List list");

  for (i = 0; i < target_bssid_list_count; i++) {
    proto_tree *list_item = NULL;
    proto_item *ppi = NULL;
    int loop_saved_offset = offset;

    list_item = proto_tree_add_subtree_format(list_tree, tvb, offset, -1, ett_target_bssid_info, &ppi, "Target BSSID List item %d", i);
    offset = dissect_target_bssid_info(tvb, pinfo, list_item, offset, "Target BSSID List");
    proto_item_set_len(ppi, offset - loop_saved_offset);
  }
  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

static int
dissect_ieee1905_steering_btm_report(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName, guint16 tlv_header_tlvlength)
{
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_ieee1905_steering_btm_report, &str_item, dispName);
  proto_tree_add_item(sub_tree, hf_ieee1905_steering_btm_report_report_bssid, tvb, offset, 6, ENC_NA);
  offset += 6;
  proto_tree_add_item(sub_tree, hf_ieee1905_steering_btm_report_reported_sta_mac_address, tvb, offset, 6, ENC_NA);
  offset += 6;
  proto_tree_add_item(sub_tree, hf_ieee1905_steering_btm_report_btm_status_code, tvb, offset, 1, ENC_NA);
  offset += 1;
  switch (tlv_header_tlvlength - 13) {
  case 6:
    proto_tree_add_item(sub_tree, hf_ieee1905_steering_btm_report_target_bssid, tvb, offset, 6, ENC_NA);
    break;

  case 0:
    break;

  default:
  // Generate exception code
    break;

  }
  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

static int
dissect_unknown_tlv_type(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName, guint16 tlv_header_tlvlength)
{
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_unknown_tlv_type, &str_item, dispName);
  proto_tree_add_item(sub_tree, hf_unknown_tlv_type_unknown_tlv_data, tvb, offset, tlv_header_tlvlength, ENC_NA);
  offset += tlv_header_tlvlength;

  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

static int
dissect_tlv_header_type(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName, guint8 *tlv_header_tlvtype, guint16 *tlv_header_tlvlength)
{
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_tlv_header_type, &str_item, dispName);
  *tlv_header_tlvtype = tvb_get_guint8(tvb, offset);
  proto_tree_add_item(sub_tree, hf_tlv_header_type_tlvtype, tvb, offset, 1, ENC_NA);
  offset += 1;
  *tlv_header_tlvlength = tvb_get_ntohs(tvb, offset);
  proto_tree_add_item(sub_tree, hf_tlv_header_type_tlvlength, tvb, offset, 2, ENC_BIG_ENDIAN);
  offset += 2;
  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

static int
dissect_protocol_tlv(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, guint offset, char *dispName)
{
  guint8 tlv_header_tlvtype;
  guint16 tlv_header_tlvlength;
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_protocol_tlv, &str_item, dispName);
  offset = dissect_tlv_header_type(tvb, pinfo, sub_tree, offset, "tlv_header", &tlv_header_tlvtype, &tlv_header_tlvlength);
  switch (tlv_header_tlvtype) {
  case IEEE1905_END_OF_MESSAGE_TLV:
    break;

  case IEEE1905_AL_MAC_ADDRESS_TYPE_TLV:
    offset = dissect_ieee1905_al_mac_address_info(tvb, pinfo, sub_tree, offset, "1905 AL MAC address type");
    break;

  case IEEE1905_MAC_ADDRESS_TYPE_TLV:
    offset = dissect_ieee1905_mac_address_info(tvb, pinfo, sub_tree, offset, "1905 MAC address type");
    break;

  case IEEE1905_DEVICE_INFORMATION_TYPE_TLV:
    offset = dissect_ieee1905_device_information(tvb, pinfo, sub_tree, offset, "1905 Device Information");
    break;

  case IEEE1905_VENDOR_SPECIFIC_TLV:
    offset = dissect_ieee1905_vendor_specific(tvb, pinfo, sub_tree, offset, "Vendor Specific", tlv_header_tlvlength);
    break;

  case IEEE1905_CHANNEL_PREFERENCE_TLV:
    offset = dissect_ieee1905_channel_preference(tvb, pinfo, sub_tree, offset, "ieee1905_channel_preference");
    break;

  case IEEE1905_STEERING_REQUEST_TLV:
    offset = dissect_ieee1905_steering_request(tvb, pinfo, sub_tree, offset, "ieee1905_steering_request");
    break;

  case IEEE1905_STEERING_BTM_REPORT_TLV:
    offset = dissect_ieee1905_steering_btm_report(tvb, pinfo, sub_tree, offset, "ieee1905_steering_btm_report", tlv_header_tlvlength);
    break;

  default:
    offset = dissect_unknown_tlv_type(tvb, pinfo, sub_tree, offset, "unknown_tlv_type", tlv_header_tlvlength);
    break;

  }
  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

static int
dissect_ieee1905_cmdu(tvbuff_t *tvb, packet_info *pinfo _U_,
        proto_tree *tree, void *data _U_)
{
  int offset = 0;
  guint8 protocoltlvs_tlv_header_tlvtype = 0;
  int i = 0;
  proto_tree *list_tree = NULL;
  proto_item *pi = NULL;
  proto_tree *sub_tree = NULL;
  proto_item *str_item = NULL;
  int saved_offset = offset;

  col_set_str(pinfo->cinfo, COL_PROTOCOL, "ieee1905");
  sub_tree = proto_tree_add_subtree(tree, tvb, offset, -1, ett_ieee1905_cmdu, &str_item, "ieee1905_cmdu");
  proto_tree_add_item(sub_tree, hf_ieee1905_cmdu_messageversion, tvb, offset, 1, ENC_NA);
  offset += 1;
  proto_tree_add_item(sub_tree, hf_ieee1905_cmdu_reserved, tvb, offset, 1, ENC_NA);
  offset += 1;
  proto_tree_add_item(sub_tree, hf_ieee1905_cmdu_messagetype, tvb, offset, 2, ENC_NA);
  offset += 2;
  proto_tree_add_item(sub_tree, hf_ieee1905_cmdu_messageid, tvb, offset, 2, ENC_BIG_ENDIAN);
  offset += 2;
  proto_tree_add_item(sub_tree, hf_ieee1905_cmdu_fragmentid, tvb, offset, 1, ENC_NA);
  offset += 1;
  proto_tree_add_item(sub_tree, hf_ieee1905_cmdu_5_lastfragmentindicator, tvb, offset, 1, ENC_NA);
  proto_tree_add_item(sub_tree, hf_ieee1905_cmdu_5_relayindicator, tvb, offset, 1, ENC_NA);
  proto_tree_add_item(sub_tree, hf_ieee1905_cmdu_5_reserved, tvb, offset, 1, ENC_NA);
  offset += 1;
  list_tree = proto_tree_add_subtree(sub_tree, tvb, offset, -1, ett_protocol_tlv_list, &pi, "ProtocolTlvs list");

  protocoltlvs_tlv_header_tlvtype = tvb_get_guint8(tvb, offset);
  while (protocoltlvs_tlv_header_tlvtype != 0) {
    proto_tree *list_item = NULL;
    proto_item *ppi = NULL;
    int loop_saved_offset = offset;

    list_item = proto_tree_add_subtree_format(list_tree, tvb, offset, -1, ett_protocol_tlv, &ppi, "ProtocolTlvs item %d", i);
    offset = dissect_protocol_tlv(tvb, pinfo, list_item, offset, "ProtocolTlvs");
    i++;
    protocoltlvs_tlv_header_tlvtype = tvb_get_guint8(tvb, offset);
    proto_item_set_len(ppi, offset - loop_saved_offset);
  }
  offset = dissect_protocol_tlv(tvb, pinfo, sub_tree, offset, "End Of Mesage");
  proto_item_set_len(str_item, offset - saved_offset);
  return offset;
}

void
proto_register_ieee1905(void)
{
  static hf_register_info hf[] = {
    {&hf_ieee1905_cmdu_messageversion,
     {"messageVersion", "ieee1905.ieee1905_cmdu.messageversion",
      FT_UINT8, BASE_DEC|BASE_RANGE_STRING, RVALS(message_version_enum_rvals), 0, NULL, HFILL }},

    {&hf_ieee1905_cmdu_reserved,
     {"reserved", "ieee1905.ieee1905_cmdu.reserved",
      FT_UINT8, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_cmdu_messagetype,
     {"messageType", "ieee1905.ieee1905_cmdu.messagetype",
      FT_UINT16, BASE_DEC|BASE_RANGE_STRING, RVALS(message_type_enum_rvals), 0, NULL, HFILL }},

    {&hf_ieee1905_cmdu_messageid,
     {"messageId", "ieee1905.ieee1905_cmdu.messageid",
      FT_UINT16, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_cmdu_fragmentid,
     {"fragmentId", "ieee1905.ieee1905_cmdu.fragmentid",
      FT_UINT8, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_cmdu_5_lastfragmentindicator,
     {"lastFragmentIndicator", "ieee1905.ieee1905_cmdu.lastfragmentindicator",
      FT_BOOLEAN, 8, TFS(&last_fragment_enum_tfs), 0, NULL, HFILL }},

    {&hf_ieee1905_cmdu_5_relayindicator,
     {"relayIndicator", "ieee1905.ieee1905_cmdu.relayindicator",
      FT_BOOLEAN, 8, TFS(&relay_indicator_enum_tfs), 0, NULL, HFILL }},

    {&hf_ieee1905_cmdu_5_reserved,
     {"reserved", "ieee1905.ieee1905_cmdu.reserved",
      FT_UINT8, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_tlv_header_type_tlvtype,
     {"tlvType", "ieee1905_cmdu.tlvtype",
      FT_UINT8, BASE_DEC|BASE_RANGE_STRING, RVALS(tlv_type_enum_rvals), 0, NULL, HFILL }},

    {&hf_tlv_header_type_tlvlength,
     {"tlvLength", "ieee1905_cmdu.tlvlength",
      FT_UINT16, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_al_mac_address_info_1905_al_mac_address_of_transmitting_device,
     {"1905 AL MAC address of transmitting device", "ieee1905_cmdu.1905_al_mac_address_of_transmitting_device",
      FT_ETHER, BASE_NONE, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_mac_address_info_mac_address_of_transmitting_interface,
     {"MAC address of transmitting interface", "ieee1905_cmdu.mac_address_of_transmitting_interface",
      FT_ETHER, BASE_NONE, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_device_information_1905_al_mac_address_of_device,
     {"1905 AL MAC address of device", "ieee1905_cmdu.1905_al_mac_address_of_device",
      FT_ETHER, BASE_NONE, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_device_information_local_interface_count,
     {"Local interface count", "ieee1905_cmdu.local_interface_count",
      FT_UINT8, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_local_interface_details_local_interface,
     {"Local interface", "ieee1905_device_information.local_interface",
      FT_ETHER, BASE_NONE, NULL, 0, NULL, HFILL }},

    {&hf_local_interface_details_media_type,
     {"Media type", "ieee1905_device_information.media_type",
      FT_UINT16, BASE_DEC|BASE_RANGE_STRING, RVALS(media_type_rvals), 0, NULL, HFILL }},

    {&hf_local_interface_details_media_specific_info_len,
     {"Media specific info len", "ieee1905_device_information.media_specific_info_len",
      FT_UINT8, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_local_interface_details_media_specific_information,
     {"Media specific information", "ieee1905_device_information.media_specific_information",
      FT_BYTES, BASE_NONE, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_vendor_specific_vendor_specific_oui,
     {"Vendor specific OUI", "ieee1905_cmdu.vendor_specific_oui",
      FT_UINT24, BASE_OUI, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_vendor_specific_vendor_specific_information,
     {"Vendor specific information", "ieee1905_cmdu.vendor_specific_information",
      FT_BYTES, BASE_NONE, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_channel_preference_radio_unique_identifier,
     {"Radio unique identifier", "ieee1905_cmdu.radio_unique_identifier",
      FT_BYTES, BASE_NONE, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_channel_preference_operating_classes,
     {"Operating classes", "ieee1905_cmdu.operating_classes",
      FT_UINT8, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_channel_preference_detail_operating_class,
     {"Operating class", "ieee1905_channel_preference.operating_class",
      FT_UINT8, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_channel_preference_detail_channel_list_count,
     {"Channel list count", "ieee1905_channel_preference.channel_list_count",
      FT_UINT8, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_channel_preference_detail_channel_list,
     {"Channel list", "ieee1905_channel_preference.channel_list",
      FT_UINT8, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_channel_preference_flags_0_preference,
     {"preference", "ieee1905_channel_preference.preference",
      FT_UINT8, BASE_DEC|BASE_RANGE_STRING, RVALS(channel_preference_prefs_rvals), 0, NULL, HFILL }},

    {&hf_channel_preference_flags_0_reason,
     {"reason", "ieee1905_channel_preference.reason",
      FT_UINT8, BASE_DEC|BASE_RANGE_STRING, RVALS(channel_preference_reason_rvals), 0, NULL, HFILL }},

    {&hf_ieee1905_steering_request_source_bss_bssid,
     {"Source BSS BSSID", "ieee1905_cmdu.source_bss_bssid",
      FT_BYTES, BASE_NONE, NULL, 0, NULL, HFILL }},

    {&hf_steering_request_flags_0_request_mode,
     {"Request Mode", "ieee1905_cmdu.request_mode",
      FT_BOOLEAN, 8, TFS(&steering_request_mode_tfs), 0, NULL, HFILL }},

    {&hf_steering_request_flags_0_btm_disassociation_imminent,
     {"BTM Disassociation Imminent", "ieee1905_cmdu.btm_disassociation_imminent",
      FT_BOOLEAN, 8, NULL, 0, NULL, HFILL }},

    {&hf_steering_request_flags_0_btm_abridged,
     {"BTM Abridged", "ieee1905_cmdu.btm_abridged",
      FT_BOOLEAN, 8, NULL, 0, NULL, HFILL }},

    {&hf_steering_request_flags_0_reserved,
     {"Reserved", "ieee1905_cmdu.reserved",
      FT_UINT8, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_steering_op_window_steering_opportunity_window,
     {"Steering opportunity window", "ieee1905_cmdu.steering_opportunity_window",
      FT_UINT16, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_steering_request_btm_disassociation_timer,
     {"BTM disassociation timer", "ieee1905_cmdu.btm_disassociation_timer",
      FT_UINT16, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_steering_request_sta_list_count,
     {"STA list count", "ieee1905_cmdu.sta_list_count",
      FT_UINT8, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_steering_request_target_mac_address,
     {"Target MAC address", "ieee1905_cmdu.target_mac_address",
      FT_ETHER, BASE_NONE, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_steering_request_target_bssid_list_count,
     {"Target BSSID List Count", "ieee1905_cmdu.target_bssid_list_count",
      FT_UINT8, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_target_bssid_info_target_bssid,
     {"Target BSSID", "ieee1905_steering_request.target_bssid",
      FT_BYTES, BASE_NONE, NULL, 0, NULL, HFILL }},

    {&hf_target_bssid_info_target_bssid_operating_class,
     {"Target BSSID Operating Class", "ieee1905_steering_request.target_bssid_operating_class",
      FT_UINT8, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_target_bssid_info_target_bssid_channel_number,
     {"Target BSSID Channel Number", "ieee1905_steering_request.target_bssid_channel_number",
      FT_UINT8, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_steering_btm_report_report_bssid,
     {"Report BSSID", "ieee1905_cmdu.report_bssid",
      FT_BYTES, BASE_NONE, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_steering_btm_report_reported_sta_mac_address,
     {"Reported STA MAC address", "ieee1905_cmdu.reported_sta_mac_address",
      FT_ETHER, BASE_NONE, NULL, 0, NULL, HFILL }},

    {&hf_ieee1905_steering_btm_report_btm_status_code,
     {"BTM Status Code", "ieee1905_cmdu.btm_status_code",
      FT_UINT8, BASE_DEC, NULL, 0, NULL, HFILL }},

    {&hf_unknown_tlv_type_unknown_tlv_data,
     {"Unknown TLV data", "ieee1905_cmdu.unknown_tlv_data",
      FT_BYTES, BASE_NONE, NULL, 0, NULL, HFILL }},

  };

  static gint *ett[] = {
    &ett_ieee1905_cmdu,
    &ett_protocol_tlv_list,
    &ett_protocol_tlv,
    &ett_tlv_header_type,
    &ett_ieee1905_al_mac_address_info,
    &ett_ieee1905_mac_address_info,
    &ett_ieee1905_device_information,
    &ett_local_interface_details_list,
    &ett_local_interface_details,
    &ett_ieee1905_vendor_specific,
    &ett_ieee1905_channel_preference,
    &ett_channel_preference_detail_list,
    &ett_channel_preference_detail,
    &ett_uint8_list,
    &ett_uint8,
    &ett_channel_preference_flags,
    &ett_ieee1905_steering_request,
    &ett_steering_request_flags,
    &ett_steering_op_window,
    &ett_ether_t_list,
    &ett_ether_t,
    &ett_target_bssid_info_list,
    &ett_target_bssid_info,
    &ett_ieee1905_steering_btm_report,
    &ett_unknown_tlv_type,
  };

  proto_ieee1905 = proto_register_protocol("IEEE 1905.1a", "ieee1905", "ieee1905");

  proto_register_field_array(proto_ieee1905, hf, array_length(hf));
  proto_register_subtree_array(ett, array_length(ett));
}

void
proto_reg_handoff_ieee1905(void)
{
  static dissector_handle_t ieee1905_handle;
  ieee1905_handle = create_dissector_handle(dissect_ieee1905_cmdu, proto_ieee1905);
  dissector_add_uint("ethertype", 0x893A, ieee1905_handle);
}
