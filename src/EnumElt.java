/*
 * Generate a Wireshark dissector by listening to all the enter and exit points
 * in the grammar.
 *
 * Copyright 2017, 2018 Richard Sharpe <realrichardsharpe@gmail.com>
 */

public class EnumElt implements Comparable<EnumElt> {
    String val;     // The value, as a string
    String id;      // The ID associated with the value
    String string;  // Associated string
    int definingLine;

    public EnumElt(String nVal, String nId, String nString) {
        val = nVal;
        id = nId;
        string = nString;
    }

    // Convert the vals to ints and compare. We know the vals are
    // ints or hex ints.
    public int compareTo(EnumElt b) {
        return Integer.compare(Integer.decode(val),
                               Integer.decode(b.val));
    }

    void generateEnumDecl(int maxLen) {
        System.out.println("  " +
              String.format("%-" + maxLen + "." + maxLen +"s", id)
              + " = " + val + ",");
    }

    String generateValString() {
        String retVal = null;

         if (string != null) {
             retVal = "  { " + val + ", " + string + " },";
         }

        return retVal;
    }

    String generateTfsValString() {
        String retVal = null;

        return "  " + string + ",";
    }

    String generateRangeString(int prevVal, String defaultVal) {
        String retVal = "";
        int curVal = Integer.decode(val);

        if (curVal > (prevVal + 1)) {
            retVal = "  { " + (prevVal + 1) + ", " + (curVal - 1) +
                     ", " + defaultVal + " },\n";
        }

        return retVal + "  { " + val + ", " + val + ", " + string +
                        " },";
    }
}
