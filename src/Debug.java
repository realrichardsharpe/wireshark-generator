/*
 * Generate a Wireshark dissector by listening to all the enter and exit points
 * in the grammar.
 *
 * Copyright 2017, 2018 Richard Sharpe <realrichardsharpe@gmail.com>
 */

import java.io.InputStream;
import java.util.*;

class Debug {
    static int debugLevel = 0;

    static String indent = "";

    static  void setDebugLevel(int level) {
        debugLevel = level;
    }

    static void println(int level, String debugLine) {
        if (debugLevel >= level)
            System.out.println(indent + debugLine);
    }

    static void print(int level, String debugLine) {
        if (debugLevel >= level)
            System.out.print(indent + debugLine);
    }
}

