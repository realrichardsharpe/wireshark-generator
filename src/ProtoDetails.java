/*
 * Generate a Wireshark dissector by listening to all the enter and exit points
 * in the grammar.
 *
 * Copyright 2017, 2018 Richard Sharpe <realrichardsharpe@gmail.com>
 */

import java.util.*;

class ProtoDetails {
    String name;
    String shortName;
    String abbrev;
    String dissector_entry;
    String dissector_table;
    String dissector_index;
    String tableType;

	    public ProtoDetails(String pName, String pShortNname, String pAbbrev) {
        name = pName;
        shortName = pShortNname;
        abbrev = pAbbrev;
        tableType = "uint";
    }

    public void setDissectorEntry(String dissector_entry) {
        this.dissector_entry = dissector_entry;
    }

    public void setDissectorTable(String dissector_table) {
        this.dissector_table = dissector_table;
    }

    public void setDissectorIndex(String dissector_index) {
        this.dissector_index = dissector_index;
    }

    public void setDissectorTableType(String tableType) {
        this.tableType = tableType;
    }
}
