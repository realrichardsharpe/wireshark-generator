/*
 * Generate a Wireshark dissector by listening to all the enter and exit points
 * in the grammar.
 *
 * Copyright 2017, 2018, 2019 Richard Sharpe <realrichardsharpe@gmail.com>
 *
 * vim: set tabstop=4 shiftwidth=4 ai expandtab
 */
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.*;

enum EltType {
    ELT_EXTERNAL,         // An External type?
    ELT_BASE,
    ELT_SUB_STRUCT,       // Base split into fields
    ELT_ARRAY,
    ELT_UNION,
    ELT_STRUCT,
    ELT_ENUM
}

class Generator extends WiresharkGeneratorBaseListener {

    public Generator(String inputFile) {
        this.inputFile = inputFile;
    }

    public Map<String, StructInfo> getStructs() {
        return structs;
    }

    public Map<String, EnumInfo> getEnums() {
        return enums;
    }

    String inputFile;

    // Defines an Array elt.
    public class ArrayElt extends Elt {
        EltType eltType;  // Not sure this is needed ...
        String type;
        int eltCount;         // Fixed Length. If Zero, next controls
        Expression arrayCntl; // The control element name.
        int definingLine;
        Boolean ettDeclared;
        Boolean ettDefined;

        public ArrayElt(String name) {
            this.name = name;
            eltCount = 0;
            ettDeclared = false;
            ettDefined = false;
        }

        public void print() {
            Debug.print(1, "/* " + type + " " + name + "[");
            Debug.print(1, arrayCntl.printExpression());
            Debug.println(1, "] */");

        }

        public String printDebugName() {
            return name;
        }

        Elt getEltIf(String eltName) {
            if (name.equals(eltName))
                return this;
            return null;
        }

        // We are not a struct but our type might be ... this might need
        // to change at some time.
        Boolean isStruct() {
            Debug.println(10, "//Is ArrayElt " + name + " of type " + type +
                              " a struct?");
            StructInfo struct = structs.get(type);

            return (struct != null);
        }

        // We are not a base type
        Boolean isBaseType() {
            return false;
        }

        // We are not a function
        Boolean isFunction() {
            return false;
	    }

        // Our type is what we were defined as. However, if it is an enum
        // of a typedef, we need to resolve to that type.
        String getType() {
            EnumInfo enumInfo = enums.get(type);

            if (enumInfo != null)
                return enumInfo.type;

            TypeDef typeDef = typedefs.get(type);
            if (typeDef != null)
                return typeDef.type;

            // Else it must be a base type...
            return type;
        }

        // Return any needed fields. This means the path in the
        // arrayCntl element, if it exists. There might also be elements
        // of the struct that are needed if we are a struct
        ArrayList<ProtoPath> getNeeded() {
            ArrayList<ProtoPath> needed = new ArrayList<ProtoPath>();

            Debug.println(10, "//getNeeded for " + name);
            // We must do things in this order.
            if (isStruct()) {
                Debug.println(10, "//Setting up refs for struct: " + type);
                StructInfo struct = structs.get(type);
                struct.setupNeededReferences(needed);
                Debug.println(10, "//DONE setting up refs for struct: " +
                                   type);
            }

            if (needed.size() > 0) {
                Debug.println(1, "//There shouldn't be any refs left for " +
                                  "ArrayElt: " + type);
            }

            // This should be relative to our parent.
            // However, it could also be relative to us as well, in that
            // case we do want it because it is self-relative
            if (arrayCntl.path != null) {
                if (!arrayCntl.path.equalsFirst(name)) {
                    ProtoPath path = arrayCntl.path.duplicate();
                    //path.makeParentRelative();
                    Debug.println(10, "//Adding path " + path.printPath() +
                                  " from ArrayElt: " + name);
                    needed.add(path);
                }
            } else {
                Debug.println(10, "//Rejected " +
                                  arrayCntl.printExpression() +
                                  " because it is only a integer: " + name);
            }

            if (needed.size() > 0)
                return needed;

                return null;
            }

        // return any needed items. These are fields that are needed above
        // to append to or to apply an ExpertItem to.
        ArrayList<ProtoPath> getItemNeeded() {
            ArrayList<ProtoPath> needed = new ArrayList<ProtoPath>();

            Debug.println(10, "//getItemNeeded for " + name);
            if (isStruct()) {
                Debug.println(10, "//Setting up refs for struct: " + type);
                StructInfo struct = structs.get(type);
                struct.setupNeededItemReferences(needed);
                Debug.println(10, "//DONE setting up Item refs for struct: " +
                                   type);
            }

            if (needed.size() > 0)
                return needed;

	    return null;
        }

        // Check that our type is a base type or a struct
        // and we also need to check that the arrayCntl exists
        Boolean checkMissing(ArrayList<StructInfo> stack, String cont) {
            if (!Util.isBaseType(type) && structs.get(type) == null)
                return true;

            // We must be a member of the most recent element on the
            // stack, so we work from there.
            StructInfo struct = stack.get(stack.size() - 1);
            // Perhaps this should be handled by Expression
            // If there is no path we don't have to check.
            if (arrayCntl.path != null &&
                !struct.checkPathIsBaseType(arrayCntl.path, stack)) {
                Debug.println(10, "//Elt " + name + " has a control path " +
                                  arrayCntl.printExpression() +
                                  " that does not exist in " +
                                  struct.name + " .");
                return true;
            }

            // If it's a struct, need to check the type
            Debug.println(10, "//Checking if " + name + " is a struct");
            if (this.isStruct()) {
                struct = structs.get(type);

                Debug.println(10, "//Checking if " + name + " has missing");
                return struct.checkMissing(stack);
            }

            return false;
        }

        // If we are not an array of bytes or char types, we need
        // an iterator because we want to display it as an array, so
        // we probably also need a subtree, but we can break that out
        // later.
        Boolean needsIterator() {
            if (Util.isNonArrayType(type))
                return false;
            Debug.println(10, "//Type " + name + " needs iterator");
            return true;
        }

        // Get the name of the controlling element. It will be used
        // locally when generating code to handle this element
        // and may have been passed into the structure that this is
        // a part of. If the elt is local to the structure, it won't be.
        String getControlName() {

            return "";
        }

        // Generate the control expression to be used
        String getControlExpr() {
            Debug.println(10, "//getControlExpr called for " + name);
            return "";
        }

        // Figure out which offset into a TVB to use for the control var.
        // It will be an offset from the start of the structure that
        // defines us. Must be a struct.
        String getControlField() {
            // Eventually need to deal with things not at first offset
            Debug.println(10, "  //Getting getter for type: " +
                              arrayCntl.path.getType());
            return Util.baseTypeGetter(arrayCntl.path.getType(),
                                  endianType) + "(tvb, offset)";
        }

        // We insert a subtree and then add each element as a child of
        // the subtree. An iterator has already been provided for us.
        // However, if this is a self-referential array elt, ie, the
        // control var referrs to us, then a different loop is needed.
        void printDissector(String tree, String containerName,
                            StructInfo container) {
             printDissector(tree, containerName, container, "");
        }

        void printDissector(String tree, String containerName,
                            StructInfo container, String prefix) {
            String controlVar = arrayCntl.cntlExpression();
            Boolean refetch = false;

            // If it's a non-array type, then just insert it with a
            // length derived from the arrayCntl. ie, controlVar
            Debug.println(10, "//Array type: " + type);
            if (Util.isNonArrayType(type)) {
                Debug.println(10, "//Non array type " + controlVar +
                                  " pn: " + printName());
                // Handle special vars ...
                if (controlVar.startsWith("l_remaining_len")) {
                    System.out.println(prefix +"  l_remaining_len = " +
                                       "tvb_reported_length_remaining(" +
                                       "tvb, offset);");
                }
                System.out.println(prefix + "  proto_tree_add_item(" +
                                   tree + ", " +
                                   "hf_" + containerName + "_" +
                                   printName() + ", tvb, offset, " +
                                   controlVar + ", ENC_NA);");
                System.out.println(prefix + "  offset += " + controlVar + ";");
                //System.out.println("");
                return;
            }

            System.out.println(prefix +
                               "  list_tree = proto_tree_add_subtree(" +
                               tree + ", tvb, offset, -1, ett_" + type +
                               "_list, &pi, " +
                               Util.quoteString(name + " list") +
                               ");");
            System.out.println("");

            // The loop condition now has to depend on the arrayCntl info
            // and where we get it from.
            if (arrayCntl.path != null &&
                arrayCntl.path.equalsFirst(name)) { // Self relative
                Debug.println(10, "  //Generate a self-relative entry" +
                                  " for: " + name + "[" +
                                  arrayCntl.path.printPath() + "]");
                // We have to fetch the control var value ...
                System.out.println(prefix + "  l_" +
                                   arrayCntl.path.paramName() + " = " +
                                   getControlField() + ";");
                System.out.println("  while (" + controlVar + ") {");
                refetch = true;
            } else {
                Debug.println(10, "  //ControlVar: " +
                                  arrayCntl.printExpression());
                System.out.println(prefix + "  for (i = 0; i < " + controlVar +
                                   "; i++) {");
            }

            // If we are handling a struct, it will get a new subtree
            // automatically, so we don't need one here, but we might
            // want to modify the string it is labelled with.
            System.out.println(prefix + "    proto_tree *list_item = NULL;");
            System.out.println(prefix + "    proto_item *ppi = NULL;");
            System.out.println(prefix + "    int loop_saved_offset = offset;");
            System.out.println("");
            System.out.println(prefix +
                              "    list_item = proto_tree_add_subtree_format(" +
                               "list_tree, tvb, offset, -1, ett_" +
                               type + ", &ppi, " +
                               Util.quoteString(name + " item %d") +
                               ", i);");

            // Now, actually generate code to dissect the type.
            if (isStruct()) {
                StructInfo struct = structs.get(type);
                struct.generateDissectCall(prefix + "  ", "list_item", name);
            } else {
                System.out.println(prefix +
                                   "    proto_tree_add_item(list_item, " +
                                   "hf_" + containerName + "_" +
                                   printName() +
                                   ", tvb, offset, " +
                                   Util.baseTypeLenString(type) + ", " +
                                   Util.getEncoding(type, endianType) + ");");
                // Update offset ...
                System.out.println(prefix + "    offset += " +
                                   Util.baseTypeLenString(type) + ";");
            }

            if (refetch) {
                System.out.println(prefix + "    i++;");
                System.out.println(prefix + "    l_" +
                                   arrayCntl.path.paramName() +
                                   " = " + getControlField() + ";");
            }

            // Update the length of the list item we added
            System.out.println(prefix +
                               "    proto_item_set_len(ppi, offset - " +
                               "loop_saved_offset);");
            System.out.println(prefix + "  }");
        }

	// This may have to change if we need it as an array control and
	// a switch control etc.
        void generateInitialVars() {
            if (arrayCntl.path != null && arrayCntl.path.equalsFirst(name)) {
                System.out.println("  " + arrayCntl.path.getWsType() + " l_" +
                                   arrayCntl.path.paramName() + " = 0;");
            }
        }

        // Arrays don't have their own hf vars, but the base type does if
        // not a struct.
        void generateHfDecl(String cont) {
            if (!isStruct()) {
                if (cont == null)
                    cont = "";
                    System.out.println("static int hf_" + cont + "_" +
                                       printName() + " = -1;");
            }
        }

        String getWsDisplayInfo() {
            String wsDisplayInfo = Util.getWsDisplayType(getType());

            // Check if we are an enum, then we may have to add more
            EnumInfo enumInfo = enums.get(type);

            if (enumInfo != null)
                return wsDisplayInfo + enumInfo.getWsDisplayInfo();
            else
                return wsDisplayInfo;
        }

        // We could be an array of structs with switches.
        Boolean generateEiDecl() {
            if (isStruct()) {
                StructInfo struct = structs.get(type);
                return struct.generateEiDecl();
            }
            return false;
        }

        void generateHfArray(String cont, String searchName) {
            if (isStruct()) {
                 StructInfo struct = structs.get(type);

                 struct.generateHfArray(cont);
            } else {
                if (cont == null)
                    cont = "";
                Debug.println(10, "//Generating hf array for " + cont + " and " +
                                   printName());
                CodeGen.genHfEntry("hf_" + cont + "_" + printName(),
                                   name,
                                   searchName + "." + printName(),
                                   Util.getWsFieldType(getType()),
                                   getWsDisplayInfo(),
                                   Util.getWsFieldConvert(type),
                                   "0",
                                   null,
                                   "HFILL");
            }
        }

        void generateEttDecl() {
            StructInfo struct = structs.get(type);

            // If it's a base type, this can fail.
            if (struct == null) {
                Debug.println(10, "//Should not happen. Could not find " +
                                  "StructInfo for " + type);
            }
            if (!Util.isNonArrayType(type) && !ettDeclared) {
                System.out.println("static gint ett_" + type +
                                   "_list = -1;");

                // This is a hack and could cause problems.
                if (struct == null || !struct.ettDeclared) {
                    System.out.println("static gint ett_" + type +
                                       " = -1;");
                }
                if (isStruct()) {
                    struct = structs.get(type);

                    struct.generateEttDecl();
                    struct.ettDeclared = true;
                }
                ettDeclared = true;
            }
        }

        void generateEttArray() {
            Debug.println(10, "//Handling ett array for " + name +
                              " of type " + type);
            StructInfo struct = structs.get(type);

            if (struct == null) {
                Debug.println(10, "//Should not happen. Could not find " +
                                  "StructInfo for " + type);
            }
            if (!Util.isNonArrayType(type) && !ettDefined) {
                System.out.println("    &ett_" + type + "_list,");
                struct = structs.get(type);
                if (struct != null) {
                //if (!struct.ettDefined)
                //    System.out.println("    &ett_" + type + ",");
                    struct.generateEttArray();
                //struct.ettDefined = true;
                } else { // It's not a struct
                    System.out.println("    &ett_" + type + ",");
                }
                ettDefined = true;
            }
        }

        // Generate the EI Array. Nothing to do here yet.
        void generateEiArray(String searchPrefix) {
            if (isStruct()) {
                 StructInfo struct = structs.get(type);

                 struct.generateEiArrayEntries(searchPrefix);
            }
        }

        // Generate the Lua fields for this. If it is an array of structs
        // then we don't need anything because the structs will have their
        // own fields. Otherwise we need a field, and a string or bytestring
        // will need a field.
        void generateLuaFieldDef(String fieldPrefix, String searchPrefix,
                                 ArrayList<String> structsLeft,
                                 ArrayList<String> fieldNames,
                                 ArrayList<String> expertNames) {
            if (isStruct()) {
                // Nothing to do, I think
            } else {
                String fieldName = "f_" + fieldPrefix + "_" + printName();
                System.out.print("local " + fieldName + " = ProtoField." +
                                 Util.luaType(getType()) + "(" +
                                 Util.quoteString(searchPrefix + "." +
                                             printName()) + ", " +
                                 Util.quoteString(printName()) +
                                 ", " + Util.getLuaDisplayType(getType()));
                if (Util.isEnum(type))
                    System.out.print(", " + type);
                System.out.println(")");
                fieldNames.add(fieldName);
            }
        }

        void generateLuaStructDissect(String tree,
                                      String protoName,
                                      String fieldPrefix,
                                      ArrayList<String> structsLeft) {
            String controlVar = arrayCntl.cntlExpression();
            Boolean refetch = false;

            // If a non-array type, like byte, string etc then simple
            if (Util.isNonArrayType(type)) {
                if (controlVar.startsWith("l_remaining_len")) {
                    System.out.println("    l_remaining_len = " +
                                       "buffer:reported_length_remaining(" +
                                       "offset)");
                }
                Debug.println(10, "-- Using fieldPrefix = " + fieldPrefix);
                System.out.println("    " + tree + ":" +
                                   Util.getLuaAdder(getType(), endianType) +
                                   "(f_" + fieldPrefix + "_" + printName() +
                                   ", buffer(offset, " + controlVar +
                                   "))");
                System.out.println("    offset = offset + " + controlVar);
            }
        }
    }

    enum ParamType {
        PTYPE_STRING,
	PTYPE_PATH
    }

    // A class to handle params. Can be protopaths or strings, ints
    public class Param {
        ParamType paramType;
	String stringParam;
	ProtoPath pathParam;

	public Param(String param) {
	    paramType = ParamType.PTYPE_STRING;
	    stringParam = param;
	}

	public Param(WiresharkGeneratorParser.FieldPathContext path) {
	    paramType = ParamType.PTYPE_PATH;
	    pathParam = new ProtoPath(path);
	}

	public String printString() {
	    switch (paramType) {
            case PTYPE_STRING:
		return stringParam;
            case PTYPE_PATH:
		return pathParam.printPath();
	    }
	    return ""; // Return required by the compiler.
	}
    }

    // This class encodes exception/function types
    // It includes multiple arguments.
    // These may have to be different/overridden by different generators
    public class FunctionElt extends Elt {
        EltType eltType;  // Not sure this is needed ...
        int definingLine;
        Boolean ettDeclared;
	    Boolean ettDefined;
	    ArrayList<Param> params;

	    public FunctionElt() {
            params = new ArrayList<Param>();
	    }

        void print() {
            Debug.print(1, "/* " + name + "(");
	        for (Param param:params) {
                Debug.print(1, param.printString() + ",");
	        }
            Debug.println(1, ") */");

	    }

        public String printDebugName() {
            return name;
        }

	    Elt getEltIf(String eltName) {
            if (name.equals(eltName))
                return this;
            return null;
	    }

        Boolean checkMissing(ArrayList<StructInfo> stack, String cont) {
	        return false;
        }

        Boolean isBaseType() {
	        return false;
        }

	    Boolean isStruct() {
            return false;
        }

        Boolean isFunction() {
            Debug.println(10, "//Returning true for function " + name);
            return true;
        }

	    // What is our type?
        String getType() {
            return "";
        }

        Boolean needsIterator() {
            return false;
        }

        void generateInitialVars() {

        }

        void generateHfDecl(String cont) {

        }

        // May need to be smarter here
        Boolean generateEiDecl() {
            System.out.println("static expert_field " +
                               expertInfoVarName() + " = EI_INIT;");
            return true;
        }

        void generateHfArray(String cont, String searchName) {

        }

        void generateEttDecl() {

        }

        void generateEttArray() {

        }

        // Generate the EI Array
        void generateEiArray(String searchPrefix) {
            String name = Util.printName(searchPrefix);
            System.out.println("    {&" + expertInfoVarName() + ",");
            System.out.println("      {\"" + name + "." +
                               params.get(1).printString() + "\", " +
                               params.get(2).printString() + ", " +
                               params.get(3).printString() + ",");
            System.out.println("       " + params.get(4).printString() +
                               ", EXPFILL }},");
        }

    	String expertInfoVarName() {
            return "ei_" + params.get(0).pathParam.paramName();
        }

	    // This needs to distinguish between the various functions ...
        void printDissector(String tree, String containerName,
                            StructInfo container) {
            Debug.print(10, "// Need something for function: " + name);
            Debug.print(10, "(");
            for (Param param:params) {
                Debug.print(10, param.printString() + ", ");
            }
            Debug.println(10, ")");
            System.out.println("    expert_add_info_format(pinfo, i_" +
                               params.get(0).pathParam.paramName() + ", &" +
                               expertInfoVarName() + ", " +
                               params.get(4).printString() + ");");
        }

        // Go through the params look for ProtoPaths, because they
        // will be needed as items.
        ArrayList<ProtoPath> getItemNeeded() {
            ArrayList<ProtoPath> needed = new ArrayList<ProtoPath>();
            for (Param param:params) {
                if (param.paramType == ParamType.PTYPE_PATH) {
                    Debug.println(10, "// Adding " + param.printString() +
                                      " in getItemNeeded for function " +
                                      name);
                    needed.add(param.pathParam);
                }
            }
            return needed.size() > 0 ? needed : null;
        }

        ArrayList<ProtoPath> getNeeded() {
            return null;
        }

        String getWsDisplayInfo() {
            return null;
        }

        // Generate the protoExpert for Lua
        void generateLuaFieldDef(String fieldPrefix, String searchPrefix,
                                 ArrayList<String> structsLeft,
                                 ArrayList<String> fieldNames,
                                 ArrayList<String> expertNames) {
            String expertName = "ei_" + params.get(0).pathParam.paramName();
            System.out.println("local " + expertName +
                               " = ProtoExpert.new(" + "\"" + fieldPrefix +
                               "." + params.get(1).printString() + "\", " +
                               params.get(4).printString() + ", expert.group." +
                               params.get(2).printString().substring(3) +
                               ", expert.severity." +
                               params.get(3).printString().substring(3) +
                               ")");
            expertNames.add(expertName);
        }

        // treeitem:add_proto_expert_info(expert, [text])
        void generateLuaStructDissect(String tree,
                                      String protoName,
                                      String prefixName,
                                      ArrayList<String> structsLeft) {
            System.out.println("    i_" + params.get(0).pathParam.paramName() +
                               ":add_proto_expert_info(ei_" +
                               params.get(0).pathParam.paramName() + ", " +
                               params.get(4).printString() + ")");
        }
    }

    // This class encodes the arms of a case statement. It can be:
    //
    // 1. A default value.
    // 2. A normal case value, with a type or 'void'
    // 3. An exception (exception, warning, debug ...)
    public class UnionCaseDecl {
        Boolean isDefault;         // If default, no case value
        String caseValue;
        Boolean isVoid;            // If void, no type ...
        String typeName;
        String displayName;        // Field name for case arm
        String localName;          // If it's a base type. Used for hf ...
        //Boolean isArray;           // If it's an array
        //ArrayElt array;            // If it's an array
        ArrayList<Elt> elts;
        int definingLine;

        public UnionCaseDecl() {
            isDefault = false;
            isVoid = false;
            elts = new ArrayList<Elt>();
        }

        public String printDebugName() {
            if (isDefault)
                return "DEFAULT";
            else
                return caseValue;
        }

        // Should we check that the caseValue, if not a number, matches
        // one of the values in an enum?
        Boolean checkMissing(ArrayList<StructInfo> stack, String cont) {
            Debug.println(10, "//UnionCaseDecl Checking for missing in: " +
                              typeName + " displayName: " + displayName);

            if (isVoid) {
                Debug.println(10, "//The caseValue " + caseValue +
                                  " is void.");
                return false;
            }

            if (typeName != null && typeName.equals("exception"))
                return false;

	        if (elts.size() > 0) {
                for (Elt elt:elts) {
                    if (elt.checkMissing(stack, cont)) // Should check all.
                        return true;
                }
	            return false;
            }

            if (isBaseType()) {
                Debug.println(10, "//The caseValue " +
                                  (caseValue != null ? caseValue : "default") +
                                  " is a base type.");
                Debug.println(10, "//  Returning false");
                return false;
            }

            Debug.println(10, "//Checking  if there is a typedef called: " +
                              typeName);
            // Now check if it is a typedef, struct or enum
            TypeDef typeDef = typedefs.get(typeName);

            if (typeDef != null) {
                Debug.println(10, "//Checking if typedef " + typeName +
                                  " is missing");
                return typeDef.checkMissing();
            }

            Debug.println(10, "//Checking if there is a struct called: " +
                              typeName);
            StructInfo struct = structs.get(typeName);
            if (struct != null) {
                return struct.checkMissing(stack);
            }

            Debug.println(10, "//Checking if there is an enum called: " +
                              typeName);

            EnumInfo enumInfo = enums.get(typeName);
            if (enumInfo != null)
                return false;

            Debug.println(10, "//Type " + typeName + " seems to be " +
                              "missing");
            return true;
        }

        // If any of the elts are structs, we are a struct
        Boolean isStruct() {
            Debug.println(10, "//Checking if CaseDecl " + localName +
                              " is a struct");
            // This needs fixing because we have a list of elts now.
            return (structs.get(typeName) != null);
        }

        // If we resolve to a function, call it's getItemNeeded funtion.
        // For the moment only switch statements will have them
        ArrayList<ProtoPath> getItemNeeded() {
            ArrayList<ProtoPath> needed = new ArrayList<ProtoPath>();
            Debug.println(10, "//Checking for items needed in " +
                              printDebugName());
            for (Elt elt:elts) {
                Debug.println(10, "//Calling getItemNeeded for " +
                                  elt.printDebugName());
                ArrayList<ProtoPath> localNeeded = elt.getItemNeeded();
                if (localNeeded != null)
                    needed.addAll(localNeeded);
            }
            return needed.size() > 0 ? needed : null;
        }

        // If we resolve to a struct, call it's getNeeded list?
        ArrayList<ProtoPath> getNeeded() {
            ArrayList<ProtoPath> needed = new ArrayList<ProtoPath>();
            for (Elt elt:elts) {
                ArrayList<ProtoPath> eltNeeded = new ArrayList<ProtoPath>();
                Debug.println(10, "//Getting needed for: " +
                                  elt.name + ":" + elt.getType());
                eltNeeded = elt.getNeeded();
                if (eltNeeded != null)
                    needed.addAll(eltNeeded);
            }
            return needed;
        }

        // Are we a base type? Check our type in the list of base types
        // This needs to share code with others doing the same.
        Boolean isBaseType() {
            if (Util.isBaseType(typeName)) {
                return true;
            } else {
                // Check to see if it is a typedef that resolves or an enum
                TypeDef typeDef = typedefs.get(typeName);

                if (typeDef != null) {
                   return typeDef.isBaseType();
                }

                // Check if it is an enum
                EnumInfo enumInfo = enums.get(typeName);

                if (enumInfo != null) {
                    return enumInfo.isBaseType();
                }

                // Check to see if it is a struct with a single entry that
                // is a base type. This seems wrong. July 18.
                StructInfo struct = structs.get(typeName);

                if (struct != null) {
                    return false;
                    //return struct.isBaseType();
                }

                // FIXME. List line number here.
                Debug.println(10, "//UnionCaseDecl type \"" + typeName +
                                  "\" is not a base type! isVoid = " + isVoid);
                Debug.println(10, "//length of typeName is " +
                                  typeName.length());
                return false;
            }
        }

        // Our type is what we were defined as.
        String getType() {
            EnumInfo enumInfo = enums.get(typeName);

            if (enumInfo != null)
                return enumInfo.type;

            TypeDef typeDef = typedefs.get(typeName);
            if (typeDef != null)
                return typeDef.type;

            return typeName;
        }

        Boolean needsIterator() {
            return false;
        }

        void generateInitialVars() {

        }

        Boolean generateEiDecl() {
            Boolean res = false;
            for (Elt elt:elts) {
                if (elt.generateEiDecl())
                    res = true;
            }
            return res;
        }

        void printDissector(String tree, String containerName,
                            StructInfo container) {

            if (isDefault)
                System.out.println("  default:");
            else
                System.out.println("  case " + caseValue + ":");

            // Generate the dissection. If void, nothing to do, else
            // call a dissector.
            Debug.println(10, "  // Handling case arm for " +
                              (displayName != null ? displayName :
                                                     printDebugName()));

            if (elts.size() > 0) {
                String resultCont = containerName + "_" +
                                        Util.printName(localName);
                for (Elt elt:elts) {
                    Debug.println(10, "// Generating dissector elt with: " +
                                      resultCont);
                    elt.printDissector(tree, resultCont, container);
                }
                System.out.println("    break;");
                return;
            }
            // If it's a void nothing to do. Clean this up.
            if (isVoid) {
                Debug.println(10, "// Is void for: " + localName);
                System.out.println("    break;");
                return;
            }

            if (isStruct()) {
                // Might not be a struct ...
                // but it might be an array.
                Debug.println(10, "// Case arm typeName: " + typeName);
                StructInfo struct = structs.get(typeName);

                // Must pass the display name if there is one.
                if (struct != null)
                    struct.generateDissectCall("  ", tree,
                                               displayName != null ?
                                                   displayName : typeName);
            } else if (isBaseType()) {
                Debug.println(10, "// Is a base type: " + typeName +
                                  " pn: " + containerName);
                System.out.println("    proto_tree_add_item(" + tree +
                                   ", hf_" + containerName + "_" +
                                   Util.printName(localName) +
                                   ", tvb, offset, " +
                                   Util.baseTypeLenString(getType()) + ", " +
                                   Util.getEncoding(getType(), endianType) +
                                   ");");
                System.out.println("    offset += " +
                                   Util.baseTypeLenString(getType()) + ";");
            } else {  // Check if a typedef
                Debug.println(10, "// Need to check typedef: " + typeName);
                Debug.println(10, "// isVoid = " + isVoid);
                TypeDef typeDef = typedefs.get(typeName);

                if (typeDef != null)
                    typeDef.printDissector(tree, container,
                                           displayName != null ?
                                                  displayName : localName);
            }

            System.out.println("    break;");
            System.out.println("");
        }

        // If the field is a base type, needs an hf field.
        void generateHfDecl(String cont) {
            String resultCont = cont + "_" + Util.printName(localName);
            if (isVoid)
                return;
            if (elts.size() > 0) {
                for (Elt elt:elts) {
                    elt.generateHfDecl(resultCont);
                }
                return;
            }
            if (isBaseType()) {
                if (cont == null)
                    cont = "";
                System.out.println("static int hf_" + cont + "_" +
                                   Util.printName(localName) + " = -1;");
            } else { // Check for typedef
                TypeDef typeDef = typedefs.get(typeName);

                if (typeDef != null) {
                    System.out.println("static int hf_" + cont + "_" +
                                       Util.printName(displayName != null ?
                                                     displayName :
                                                     localName) +
                                       " = -1;");
                }
            }
        }

        String getWsDisplayInfo() {
            String wsDisplayInfo = Util.getWsDisplayType(typeName);

            // Check if we are an enum, then we may have to add more
            EnumInfo enumInfo = enums.get(typeName);

            if (enumInfo != null) {
                wsDisplayInfo = Util.getWsDisplayType(enumInfo.getType());
                return wsDisplayInfo + enumInfo.getWsDisplayInfo();
            }
            else
                return wsDisplayInfo;
        }

        void generateHfArray(String cont, String searchName) {
            Debug.println(10, "//Generating hf array for case " +
                              (caseValue != null ? caseValue : "default") +
                              " container: " + cont + " search: " +
                              searchName);
            if (isVoid)
                return; // Done here
            if (elts.size() > 0) {
                String resultCont = cont + "_" + Util.printName(localName);
                // Add the case name to the search name ...
                String resultSearchName;
                if (isDefault)
                    resultSearchName = searchName + ".default";
                else
                    resultSearchName = searchName + "." + caseValue;
                for (Elt elt:elts) {
                    Debug.println(10, "//Generating hf array for elt in case " +
                                      " for etc: " + elt.name);
                    elt.generateHfArray(resultCont, resultSearchName);
                }
                return; // Done here
            }
            Debug.println(10, "//Generating hf array for case " +
                              (caseValue != null ? caseValue : "default") +
                              "but not a list of elts ...");
            if (isBaseType()) {
                if (cont == null)
                    cont = "";
                Debug.println(10, "//Generating hf for " + typeName);
                CodeGen.genHfEntry("hf_" + cont + "_" +
                                   Util.printName(localName),
                                   displayName != null ? displayName :
                                                        typeName,
                                   searchName + "." +
                                   Util.printName(displayName),
                                   Util.getWsFieldType(typeName),
                                   getWsDisplayInfo(),
                                   Util.getWsFieldConvert(typeName),
                                   "0",
                                   null,
                                   "HFILL");
            } else if (isStruct()) {
                StructInfo struct = structs.get(typeName);
                Debug.println(10, "//Generating hf for " + typeName +
                                  " as structure");
                struct.generateHfArray(searchName);
            } else {
                Debug.println(10, "//Should generate something here ...");
            }
        }

        // We need to generate ETT array elements in some cases
        void generateEttArray() {
            Debug.println(10, "//Generating ETT array for " + typeName);
            for (Elt elt:elts) {
                elt.generateEttArray();
            }
        }

        // Generate an EI array. Our children may need it.
        void generateEiArray(String searchPrefix) {
            for (Elt elt:elts) {
                elt.generateEiArray(searchPrefix);
            }
        }

        void generateLuaFieldDef(String fieldPrefix, String searchPrefix,
                                 ArrayList<String> structsLeft,
                                 ArrayList<String> fieldNames,
                                 ArrayList<String> expertNames) {
            if (isVoid)
                return; // Done here
            if (elts.size() > 0) {
                String fullName = fieldPrefix + "_" + caseValue;
                String newSearch = searchPrefix + "." + caseValue;
                Debug.println(10, "-- Adding decl for elts for " + fullName);
                for (Elt elt:elts) {
                    elt.generateLuaFieldDef(fullName, newSearch, structsLeft,
                                            fieldNames, expertNames);
                }
                return; // Done here
            }
            if (isBaseType()) {
                // What if it does not have a name?
                Debug.println(10, "-- Adding decl for base type " +
                                  displayName);
                String fieldName = "f_" + fieldPrefix + "_" + displayName;
                System.out.println("local " + fieldName + " = ProtoField." +
                                   Util.luaType(getType()) + "(" +
                                   Util.quoteString(searchPrefix + "." +
                                               caseValue + "." +
                                               Util.printName(displayName)) +
                                   ", " + Util.quoteString(displayName) +
                                   ", " + Util.getLuaDisplayType(getType()));
                if (Util.isEnum(typeName))
                    System.out.print(", " + typeName);
                System.out.println(")");
                fieldNames.add(fieldName);
            } else if (isStruct()) {
                structsLeft.add(typeName);
            }
        }

        // first and last are needed so we generate the correct checking
        // code since LUA does not have case statements. We also need to
        // generate a check for void to prevent a default from adding
        // fields when they should be void.
        void generateLuaStructDissect(String tree,
                                      String contName,
                                      String prefixName,
                                      String controlVar,
                                      ArrayList<String> structsLeft,
                                      Boolean isFirst,
                                      Boolean isLast) {
            if (isFirst)
                System.out.println("    if " + controlVar + " == " +
                                   caseValue +
                                   " then");
            else if (isDefault)
                System.out.println("    else");
            else
                System.out.println("    elseif " + controlVar + " == " +
                                   caseValue + " then");

            String localPrefixName = prefixName + "_" + caseValue;
            if (isVoid) {
                // Do nothing
            } else if (elts.size() > 0) {
                Debug.println(10, "-- Came with prefixName " + prefixName);
                Debug.println(10, "-- Using localPrefixName " + localPrefixName
                                  + " for " + caseValue);
                Debug.println(10, "-- Generating Lua dissect call from UnionCaseDecl for " + prefixName);
                for (Elt elt:elts) {
                    elt.generateLuaStructDissect(tree, contName, localPrefixName, structsLeft);
                }
            } else if (isBaseType()) {
                Debug.println(10, "-- Using contName = " + contName);
                System.out.println("        t_" + contName + ":" +
                                   Util.getLuaAdder(getType(), endianType) +
                                   "(f_" + localPrefixName +
                                   ", buffer(offset, " +
                                    Util.baseTypeLenString(getType()) + "))");
                System.out.println("        offset = offset + " +
                                    Util.baseTypeLenString(getType()));

            } else if (isStruct()) {
                structsLeft.add(typeName);
                // Have to add any parameters needed ...
                StructInfo struct = structs.get(typeName);

                Debug.println(10, "-- Generating Lua dissect call from UnionCaseDecl for " + contName);
                struct.generateLuaDissectCall("    ", contName,
                                             Util.quoteString(displayName));
            }

            // Only need an end if this is the last
            if (isLast)
                System.out.println("    end");
        }
    }

    // These are unnamed because it is the selected type that can
    // have a name. A path allows us to get to the field we depend
    // on and then we have the list of cases.
    public class UnionElt extends Elt {
        ArrayList<UnionCaseDecl> cases;
        int definingLine;
        Expression unionCntl; // The control element name.
        StructInfo parent;  // We can only be in one struct

        public UnionElt() {
            cases = new ArrayList<UnionCaseDecl>();
        }

        public void print() {

            Debug.print(10, "/*  switch (");
            Debug.print(10, unionCntl.printExpression());
            Debug.println(10, ") { */");

            for (UnionCaseDecl tmp2:cases) {
                if (tmp2.isDefault) {
                    Debug.print(10, "/*     default:");
                } else {
                    Debug.print(10, "/*     case " + tmp2.caseValue +
                                    ":");
                }
                if (tmp2.isVoid) {
                    Debug.println(10, "void */");
                } else if (tmp2.elts.size() > 0) {
                    for (Elt elt:tmp2.elts) {
                        elt.print();
                    }
                } else {
                    Debug.println(10, tmp2.typeName + " */");
                }
            }

            Debug.println(10, "/*}*/");
        }

        public String printDebugName() {
            return "switch with " + unionCntl.printExpression() +
                   " in struct " + parent.name;
        }

        Elt getEltIf(String eltName) {
            return null;  // We don't have a name
        }

        // Return any path that needs an item
        ArrayList<ProtoPath> getItemNeeded() {
            Debug.println(10, "//getItemNeeded for UnionElt ... ");
            ArrayList<ProtoPath> itemNeeded = new ArrayList<ProtoPath>();
            for (UnionCaseDecl caseDecl:cases) {
                ArrayList<ProtoPath> items;
                Debug.println(10, "//Calling getItemNeeded for " +
                                  caseDecl.printDebugName());
                items = caseDecl.getItemNeeded();
                if (items != null)
                    itemNeeded.addAll(items);
            }
            return itemNeeded.size() > 0 ? itemNeeded : null;
        }

        // Return any path that is in the unionCntl and check all the
        // cases to see if they are structs and what they need.
        ArrayList<ProtoPath> getNeeded() {
            ArrayList<ProtoPath> needed = new ArrayList<ProtoPath>();

            Debug.println(10, "//Union needs: " +
                              unionCntl.path.printPath());
            needed.add(unionCntl.path.duplicate());

            // Now check the cases
            for (UnionCaseDecl caseDecl:cases) {
                ArrayList<ProtoPath> caseNeeded = caseDecl.getNeeded();

                if (caseNeeded != null)
                    needed.addAll(caseNeeded);
            }
            return needed;
        }

        // Check that the path refers to something that exists
        // and is a base type. Then check the elts.
        Boolean checkMissing(ArrayList<StructInfo> stack, String cont) {
            Boolean missing = false;

            Debug.println(10, "//Check missing for: " +
                              unionCntl.path.printPath());
            Debug.println(10, "//Parent of union is " + parent);
            if (!parent.checkPathIsBaseType(unionCntl.path, stack)) {
                Debug.println(10, "//Path \""  +
                                   unionCntl.path.printPath() +
                                   "\" is not a base type");
                missing = true;
            }

            Debug.println(10, "//Checking for missing and unresolvable " +
                              "elements in " + cont);
            for (UnionCaseDecl tmp:cases) {
                Debug.println(10, "//UnionElt Checking for missing in " +
                              tmp.displayName + " for case: " +
                              tmp.caseValue);
                if (tmp.checkMissing(stack, cont)) {
                    missing = true;
                    System.out.println("Struct " + cont +
                                      " has an element " + tmp.caseValue +
                                      ":" + tmp.typeName +
                                      " that has a missing element");
                }
            }
            return missing;
        }

        // We are not a base type
        Boolean isBaseType() {
            return false;
        }

        // We are not a struct. But we may contain structs
        Boolean isStruct() {
            return false;
        }

        // We are not a function. But we may contain functions.
        Boolean isFunction() {
            return false;
        }

        Boolean needsIterator() {
            return false;
        }

        // We have no type, so return the empty string so it does not match
        // anything.
        String getType() {
            return "";
        }

        // Generate the code for a Union. For each arm ...
        void printDissector(String tree, String containerName,
                            StructInfo struct) {
            String controlVar = unionCntl.cntlExpression();

            if (controlVar.startsWith("l_remaining_len")) {
                System.out.println("  l_remaining_len = " +
                                   "tvb_reported_length_remaining(" +
                                   "tvb, offset);");
            }

            System.out.println("  switch (" + controlVar + ") {");

            for (UnionCaseDecl tmp:cases) {
                tmp.printDissector(tree, containerName, parent);
            }

            System.out.println("  }");
        }

        void generateInitialVars() {

        }

        // Iterate the elements and generate the appropriate declarations
        void generateHfDecl(String cont) {
            for (UnionCaseDecl ucDecl:cases) {
                ucDecl.generateHfDecl(cont);
            }
        }

        String getWsDisplayInfo() {
            return null;
        }

        Boolean generateEiDecl() {
            Boolean res = false;
            for (UnionCaseDecl caseElt:cases) {
                if (caseElt.generateEiDecl())
                    res = true;
            }
            return res;
        }

        void generateHfArray(String cont, String searchName) {
            for (UnionCaseDecl ucDecl:cases) {
                Debug.println(10, "//Calling generateHfArray for " +
                                  "ucDecl " + ucDecl.caseValue + " with " +
                                  cont + " and " + searchName);
                ucDecl.generateHfArray(cont, searchName);
            }
        }

        void generateEttDecl() {

        }

        void generateEttArray() {
            Debug.println(10, "//Generating ETT array elts for case stmt in " +
                              parent.name);
            for (UnionCaseDecl ucDecl:cases) {
                ucDecl.generateEttArray();
            }
        }

        // Generate an EI array. Our children may need to.
        void generateEiArray(String searchPrefix) {
            for (UnionCaseDecl ucDecl:cases) {
                Debug.println(10, "//Generating EI array ...");
                ucDecl.generateEiArray(searchPrefix);
            }
        }

        void generateLuaFieldDef(String fieldPrefix, String searchPrefix,
                                 ArrayList<String> structsLeft,
                                 ArrayList<String> fieldNames,
                                 ArrayList<String> expertNames) {
            for (UnionCaseDecl ucDecl:cases) {
                Debug.println(10, "//calling generateLuaField def for ucDecl: " +
                                   ucDecl.caseValue);
                ucDecl.generateLuaFieldDef(fieldPrefix, searchPrefix,
                                           structsLeft, fieldNames,
                                           expertNames);
            }
        }

        void generateLuaStructDissect(String tree,
                                      String protoName,
                                      String fieldPrefix,
                                      ArrayList<String> structsLeft) {
            String controlVar = unionCntl.cntlExpression();
            Boolean isFirst = true;
            UnionCaseDecl defaultDecl = null;

            if (controlVar.startsWith("l_remaining_len")) {
                System.out.println("    l_remaining_len = " +
                                   "buffer:reported_length_remaining(" +
                                   "offset)");
            }

            for (UnionCaseDecl ucDecl:cases) {
                if (ucDecl.isDefault) {
                    defaultDecl = ucDecl;
                } else {
                    // If there is a default that is last, we are not
                    // And if there is a default that is not last we are not
                    Boolean isLast = cases.indexOf(ucDecl) ==
                                           (cases.size() - 1) &&
                                     defaultDecl == null;
                    Debug.println(10, "//Generating for struct decl with case "
                                      + ucDecl.caseValue + " and protoName: "
                                      + protoName + " and fieldPrefix: " +
                                      fieldPrefix);
                    ucDecl.generateLuaStructDissect(tree, protoName,
                                                    fieldPrefix,
                                                    controlVar, structsLeft,
                                                    isFirst, isLast);
                    if (isFirst) {
                        isFirst = false;
                    }
                }
            }

            if (defaultDecl != null)
                defaultDecl.generateLuaStructDissect(tree, protoName,
                                                     fieldPrefix, "",
                                                     structsLeft, false, true);
            System.out.println("");
        }
    }

    // A sub-division of a simple type
    public class SubElt extends Elt {
        String type;           // Can have a type
        String parentType;     // We need the parent type
        int start, end;
        int definingLine;

        public void print() {
            Debug.print(10, "/*    :" + start);
            if (start != end) {
                Debug.print(10, "-" + end);
            }
            Debug.println(10, ":" + type + ": " + name + " */");
        }

        public String printDebugName() {
            return parentType + ":" + name + "." + type;
        }

        Elt getEltIf(String eltName) {
            if (name.equals(eltName))
                return this;
            return null;
        }

        // I don't think we are really a base type?
        Boolean isBaseType() {
            return Util.isBaseType(type);
        }

        // We are not a struct, actually, we might be.
        Boolean isStruct() {
            return false;
        }

        // Return any paths for items needed. Nothing here, I suspect.
        ArrayList<ProtoPath> getItemNeeded() {
            ArrayList<ProtoPath> needed = new ArrayList<ProtoPath>();

            Debug.println(10, "//getItemNeeded for subElt: " + name);
            if (isStruct()) {
                Debug.println(10, "//Setting up refs for struct: " + type);
                StructInfo struct = structs.get(type);
                struct.setupNeededItemReferences(needed);
                Debug.println(10, "//DONE setting up Item refs for struct: " +
                                   type);
            }

            if (needed.size() > 0)
                return needed;

            return null;
        }

        // We are not a function
        Boolean isFunction() {
            return false;
        }

        // There is nothing needed here.
        ArrayList<ProtoPath> getNeeded() {
            return null;
        }

        Boolean checkMissing(ArrayList<StructInfo> stack, String cont) {
            if (!Util.isBaseType(type))
                return false;

            Debug.println(10, "Struct " + cont + " has an element " +
                              name + " whose type (" + type +
                              ") does not resolve to a base type or " +
                              "struct");
            return true;
        }

        Boolean needsIterator() {
            return false;
        }

        // Our type is what we are defined as having.
        String getType() {
            EnumInfo enumInfo = enums.get(type);

            if (enumInfo != null)
                return enumInfo.type;

            TypeDef typeDef = typedefs.get(type);
            if (typeDef != null)
                return typeDef.type;

            return type;
        }

        // Generate the code for a single sub-element. If the elt is needed
        // above, then generate code to retrieve it first.
        void printDissector(String tree, String containerName,
                            StructInfo container) {
            if (container.isNeededVar(name)) {
                Debug.println(10, "//Inserting getter for needed var " +
                              name + " of type: " + type + "->" +
                              getType() + " endian: " + endianType);
                System.out.println("  *l_" +
                           container.getNeeded(name).paramName() +
                           " = ("
                           + Util.baseTypeGetter(getType(), endianType) +
                           "(tvb, offset)" + ( end > 0 ? " >> " + end :
                                                         "") + ")" +
                           "& ((1 << " + (start - end + 1) + ") - 1);");
            }
            Debug.println(10, "//ContainerName = " + containerName);
            System.out.println("  proto_tree_add_item(" + tree +
                               ", hf_" + containerName +
                               "_" + printName() + ", tvb, offset, " +
                               Util.baseTypeLenString(parentType) + ", " +
                               Util.getEncoding(type, endianType) + ");");
        }

        // Generate the actual lines for each sub-elt.
        void generateInitialVars() {
            System.out.println("    &hf_" + printName() + ",");
        }

        // Generate an hf for this one. It must be a base type at
        // this point
        void generateHfDecl(String cont) {
            if (cont == null)
                cont = "";
		Debug.println(10, "//Generating hf for subelt " + printName());
            System.out.println("static int hf_" + cont + "_" +
                               printName() + " = -1;");
        }

        String getWsDisplayInfo() {
            String wsDisplayInfo = Util.getWsDisplayType(getType());

            // Check if we are an enum, then we may have to add more
            EnumInfo enumInfo = enums.get(type);

            if (enumInfo != null)
                return wsDisplayInfo + enumInfo.getWsDisplayInfo();
            else
                return wsDisplayInfo;
        }

        Boolean generateEiDecl() {
            return false;
        }

        void generateHfArray(String cont, String searchName) {
            CodeGen.genHfEntry("hf_" + cont + "_" + printName(),
                               name,
                               searchName + "." + printName(),
                               Util.getWsFieldType(getType()),
                               getWsDisplayInfo(),
                               Util.getWsFieldConvert(type),
                               "0",
                               null,
                               "HFILL");
        }

        void generateEttDecl() {

        }

        void generateEttArray() {

        }

        // Generate an EI Array. Nothing to do here so far.
        void generateEiArray(String searchPrefix) {
            if (isStruct()) {
                 StructInfo struct = structs.get(type);

                 struct.generateEiArrayEntries(searchPrefix);
            }
        }

        void generateLuaFieldDef(String fieldPrefix, String searchPrefix,
                                 ArrayList<String> structsLeft,
                                 ArrayList<String> fieldNames,
                                 ArrayList<String> expertNames) {
            Debug.println(10, "-- Generating field def for subelt: start: " +
                          start + " end: " + end + " type: " + type);
            if (isStruct()) { // Not likely to be a struct, but could be
                structsLeft.add(type);
            } else if (isBaseType()) {
                String fieldName = "f_" + fieldPrefix + "_" + printName();
                System.out.print("local " + fieldName + " = ProtoField." +
                                 Util.luaType(getType()) +
                                 "(" + Util.quoteString(searchPrefix + "." +
                                                        printName()) +
                                 ", " + Util.quoteString(name) +
                                 ", " + Util.getLuaDisplayType(type));
                if (Util.isEnum(type))
                    System.out.print(", " + type);
                else
                    System.out.print(", nil");
                System.out.print(", " + (((1 << (start + 1)) - 1) -
                                        ((1 << end) - 1)));
                System.out.println(")");
                fieldNames.add(fieldName);
            } else {
                // Nothing to do here.
            }
        }

        void generateLuaStructDissect(String tree,
                                      String protoName,
                                      String prefixName,
                                      ArrayList<String> structsLeft) {
            Debug.println(10, "-- Generating code for a field type: " +
                          type);
            if (isStruct()) {
                StructInfo struct = structs.get(type);

                struct.generateLuaDissectCall("    ", protoName,
                                              Util.quoteString(printName()));
            } else {
                // Deal with the case where this field is used elsewhere
                Debug.println(10, "-- Using protoName = " + protoName);
                System.out.println("    " + tree + ":" +
                                   Util.getLuaAdder(getType(), endianType) +
                                   "(f_" + prefixName + "_" + printName() +
                                   ", buffer(offset, " +
                                   Util.baseTypeLenString(parentType) + "))");
            }
        }
    }

    // A simple type, a struct, or a set of subfields of a simple type
    public class LocalElt extends Elt {
        String type;
        Boolean isSubdivided;
        String localName;      // Name needed if we have subElts
        ArrayList<SubElt> subElts;
        int definingLine;

        public LocalElt() {
            isSubdivided = false;
        }

        public void print() {
            Debug.print(10, "/* ");
            if (isSubdivided) {
                Debug.println(10, type + " */");
                for (SubElt tmp:subElts) {
                    tmp.print();
                }
            } else {
                Debug.println(10, type + " " + name + " */");
            }
        }

        public String printDebugName() {
            return name;
        }

        // Return the matching elt
        Elt getEltIf(String eltName) {
            if (isSubdivided) {
                for (SubElt tmp:subElts) {
                    if (tmp.name.equals(eltName))
                        return tmp;
                }
                return null;
            } else if (name.equals(eltName))
                return this;
            return null;
        }

        // We are not a function today, but may be in the future
        Boolean isFunction() {
            return false;
	    }

        // Are we a struct? Lookup our type in the hashmap of structs
        Boolean isStruct() {
            Debug.println(10, "//Checking if elt: " + name +
                              " is a struct");
            return (structs.get(type) != null);
        }

        // Are we a base type? Check our type in the list of base types
        Boolean isBaseType() {
            if (Util.isBaseType(type)) {
                return true;
            } else {
               // Check to see if it is a typedef that resolves or an enum
               TypeDef typeDef = typedefs.get(type);

               if (typeDef != null) {
                   return typeDef.isBaseType();
               }

               // Check if it is an enum
               EnumInfo enumInfo = enums.get(type);

               if (enumInfo != null) {
                 return enumInfo.isBaseType();
               }

               // Check to see if it is a struct with a single entry that
               // is a base type. This seems wrong. July 18.
               StructInfo struct = structs.get(type);

               if (struct != null) {
                   return false;
                   //return struct.isBaseType();
               }

               // FIXME. List line number here.
               Debug.println(10, "//Type \"" + type +
                                 "\" is not a base type!");
               return false;
            }
        }

        // Our type is what we were defined as.
        String getType() {
            EnumInfo enumInfo = enums.get(type);

            if (enumInfo != null)
                return enumInfo.type;

            TypeDef typeDef = typedefs.get(type);
            if (typeDef != null)
                return typeDef.type;

            return type;
        }

        // Return any paths for items needed
        ArrayList<ProtoPath> getItemNeeded() {
            ArrayList<ProtoPath> needed = new ArrayList<ProtoPath>();

            Debug.println(10, "//getItemNeeded for LocalElt: " + name);
            if (isStruct()) {
                Debug.println(10, "//Setting up refs for struct: " + type);
                StructInfo struct = structs.get(type);
                struct.setupNeededItemReferences(needed);
                Debug.println(10, "//DONE setting up Item refs for struct: " +
                                   type);
            }

            if (needed.size() > 0)
                return needed;

            return null;
        }

        ArrayList<ProtoPath> getNeeded() {
            if (this.isStruct()) {
                StructInfo struct = structs.get(type);
                ArrayList<ProtoPath> needed = new ArrayList<ProtoPath>();
                struct.setupNeededReferences(needed);
                return needed;
            }
            return null;
        }

        Boolean checkMissing(ArrayList<StructInfo> stack, String cont) {
            Debug.println(10, "//Checkmissing for: " + name);
            // If it's a struct, need to check the type
            if (this.isStruct()) {
                StructInfo struct = structs.get(type);

                Debug.println(10, "//Calling checkmissing for " +
                              struct.name);
                return struct.checkMissing(stack);
            }

            if (this.isBaseType()) {
                Debug.println(10, "//It's a base type " + name);
                return false;
            }

            System.out.println("Struct " + cont + " has an element " +
                               name + " whose type (" + type +
                               ") does not resolve to a base type or " +
                               "struct");
            return true;
        }

        Boolean needsIterator() {
            return false;
        }

        // Get our base type length, which may involve looking up enums,
        // typedefs, etc.
        String baseTypeLen() {
            int index = Util.typeIndex(type);

            // Check if it is an enum if it is not a base type
            if (index == -1) {
                EnumInfo tmpEnum = enums.get(type);

                if (tmpEnum != null) {
                    index = Util.typeIndex(tmpEnum.type);
                }
            }

            // If we don't have an index, do we have a typedef?
            if (index < 0) {
                TypeDef typeDef = typedefs.get(type);

                if (typeDef != null) {
                    return typeDef.getLength();
                } else {
                    Debug.println(10, "//Unable to get index for type: " +
                                     type);
                    return "Unknown";
                }
            }

            return Integer.toString(Util.baseTypeLens[index]);
        }

        // If we are a struct, then insert a call to dissect_<name> passing
        // all the necessary args, if it's a typedef ...
        // proto_tree_add_item etc ...
        //
        void printDissector(String tree, String containerName,
                            StructInfo container) {

            // If it is subdivided, handle each sub-elt
            if (isSubdivided) {
                Debug.println(10, "  // Handle subdivided " + localName);
                for (SubElt tmp:subElts) {
                    tmp.printDissector(tree, printName(localName),
                                       container);
                }
                System.out.println("  offset += " + baseTypeLen() + ";");
            } else if (isStruct()) {
			StructInfo struct = structs.get(type);

                    struct.generateDissectCall("", tree, name);
            } else if (isBaseType()) {
                /*
                 * If this item is a local variable then we need to get the
                 * value of the variable. And, if it is needed above us,
                 * then we also need to get it, but in a different manner.
                 */
                if (container.isLocalVar(name)) {
                    System.out.println("  l_" + printName(name) + " = " +
                                       Util.baseTypeGetter(getType(),
                                       endianType) +
                                       "(tvb, offset);");
                } else if (container.isNeededVar(name)) {
                    Debug.println(10, "//Inserting getter for needed var " +
                                  name + " of type: " + type + "->" +
                                  getType() + " endian: " + endianType);
                    System.out.println("  *l_" +
                               container.getNeeded(name).paramName() +
                               " = " +
                               Util.baseTypeGetter(getType(), endianType) +
                               "(tvb, offset);");
                }

                if (container.isNeededItem(name)) {
                    System.out.print("  *i_" +
                                     container.getNeededItem(name).paramName() + " = ");
                } else {
                    System.out.print("  ");
                }
                System.out.println("proto_tree_add_item(" + tree +
                                   ", hf_" + containerName +
                                   "_" + printName() + ", tvb, offset, " +
                                   baseTypeLen() + ", " +
                                   Util.getEncoding(type, endianType) +
                                   ");");
                System.out.println("  offset += " +
                                   baseTypeLen() + ";");
            } else { // If it is a typedef ... then
                TypeDef typeDef = typedefs.get(type);

                /*
                 * There are two aspects to the typedef, the type and
                 * whether or not it is an array. Also, some array types
                 * can be dealt with as base types, such as byte, char.
                 */
                if (typeDef != null) {
                    typeDef.printDissector(tree, container, codeName());
                } else {
                    System.out.println("// Type " + name + " not handled!");
                }
            }
        }

        // Nothing to do so far
        void generateInitialVars() {

        }

        // Generate an hf field for ourselves and if we have sub elts, call
        // their generate method as well
        void generateHfDecl(String cont) {
            if (cont == null)
                cont = "";
            Debug.println(10, "//Handling hfDecl for: " + name);
            if (isSubdivided) {
                Debug.println(10, "//Handling subelts ...");
                // Nothing to generate for the elt, but do the subelts
                for (SubElt subElt:subElts) {
                    subElt.generateHfDecl(printName(localName));
                }
            } else if (isStruct()) {
                StructInfo struct = structs.get(type);

                struct.generateHfDecl(cont);
            } else {
                System.out.println("static int hf_" + cont + "_" +
                                   printName() + " = -1;");
            }
        }

        String getWsDisplayInfo() {
            String wsDisplayInfo = Util.getWsDisplayType(getType());

            // Check if we are an enum, then we may have to add more
            EnumInfo enumInfo = enums.get(type);

            if (enumInfo != null)
                return wsDisplayInfo + enumInfo.getWsDisplayInfo();
            else
                return wsDisplayInfo;
        }

        Boolean generateEiDecl() {
            return false;
        }

        void generateHfArray(String cont, String searchName) {
            //if (cont == null) {
            //    cont = "";
            //}
            if (isSubdivided) {
                Debug.println(10, "//Generating hf array for " + localName);
                // Nothing to generate for the elt, but do the subelts
                for (SubElt subElt:subElts) {
                    subElt.generateHfArray(printName(localName),
                                           searchName);
                }
            } else if (!isStruct()) {
                Debug.println(10, "//generating hf array for " + printName() +
                                  " with searchName " + searchName);
                CodeGen.genHfEntry("hf_" + cont + "_" + printName(),
                                   name,
                                   searchName + "." + printName(),
                                   Util.getWsFieldType(getType()),
                                   getWsDisplayInfo(),
                                   Util.getWsFieldConvert(type),
                                   "0",
                                   null,
                                   "HFILL");
            } else { // Must be a struct
                Debug.println(10, "//Calling generateHfArray for " +
                              name);
                StructInfo struct = structs.get(type);
                struct.generateHfArray(searchName);
            }
        }

        void generateEttDecl() {

        }

        void generateEttArray() {
            Debug.println(10, "//Generating ETT Array for " + name);
            if (isSubdivided) {

            } else if (isStruct()) {
                StructInfo struct = structs.get(type);

                Debug.println(10, "//Generating ETT Array for struct " +
                                  struct.name);
                struct.generateEttArray();

            } else {

            }
        }

        // Generate an EI Array.
        void generateEiArray(String searchPrefix) {
            if (isStruct()) {
                 StructInfo struct = structs.get(type);

                 struct.generateEiArrayEntries(searchPrefix);
            }
        }

        void generateLuaFieldDef(String fieldPrefix, String searchPrefix,
                                 ArrayList<String> structsLeft,
                                 ArrayList<String> fieldNames,
                                 ArrayList<String> expertNames) {
            if (isSubdivided) {
                Debug.println(10, "-- Generating fields for subdivided: " +
                              name);
                for (SubElt subElt:subElts) {
                    subElt.generateLuaFieldDef(fieldPrefix, searchPrefix,
                                               structsLeft, fieldNames,
                                               expertNames);
                }
            } else if (this.isStruct()) {
                Debug.println(10, "// Maybe we need to do something here " +
                                  "for " + printName() + " of type " + type);
                StructInfo struct = structs.get(type);
                String newFieldPrefix = fieldPrefix + "_" + printName();
                String newSearchPrefix = searchPrefix + "." + printName();
                struct.generateLuaStructFields(newFieldPrefix, newSearchPrefix,
                                               structsLeft, fieldNames,
                                               expertNames);
            } else {
                Debug.println(10, "//Generating Lua field for local elt " +
                                  printName());
                String fieldName = "f_" + fieldPrefix + "_" + printName();
                System.out.print("local " + fieldName + " = ProtoField." +
                                 Util.luaType(getType()) +
                                 "(" + Util.quoteString(searchPrefix + "." +
                                                        printName()) +
                                 ", " + Util.quoteString(name) +
                                 ", " + Util.getLuaDisplayType(getType()));
                if (Util.isEnum(type))
                    System.out.print(", " + type);
                System.out.println(")");
                fieldNames.add(fieldName);
            }
        }

        // protoName seems like a poor name here
        void generateLuaStructDissect(String tree,
                                      String protoName,
                                      String fieldPrefix,
                                      ArrayList<String> structsLeft) {
            Debug.println(10, "-- Handling local elt " + name + " with " +
                              "fieldPrefix " + fieldPrefix + " and " +
                              protoName);
            if (isSubdivided) {
                for (SubElt subElt:subElts) {
                    subElt.generateLuaStructDissect(tree, protoName, fieldPrefix, structsLeft);
                }
                System.out.println("    offset = offset + " +
                                   Util.baseTypeLenString(getType()));
            } else if (isStruct()) {
                StructInfo struct = structs.get(type);

                Debug.println(10, "-- Handling struct " + name);
                if (struct != null) {
                    protoName += "_" + name;
                    struct.generateLuaDissectCall("    ", tree,
                                             Util.quoteString(name));
                    structsLeft.add(type);
                }
            } else if (isBaseType()) {
                // Deal with LE fields.
                StructInfo struct = structs.get(protoName);

                Debug.println(10, "// protoName = " + protoName);
                if (struct.isLocalVar(name)) {
                    ProtoPath path = struct.getLocalVar(name);
                    System.out.println("    local l_" + path.paramName() +
                                       " = buffer(offset, " +
                                       Util.baseTypeLenString(getType()) +
                                       "):uint()");
                } else if (struct.isNeededVar(name)) {
                    ProtoPath path = struct.getNeeded(name);
                    Debug.println(10, "-- path = " + path);
                    System.out.println("    local l_" + path.paramName() +
                                       " = buffer(offset, " +
                                       Util.baseTypeLenString(getType()) +
                                       "):uint()");
                }

                // Add code to get the item name, ie i_ +n path.paramName()
                if (struct.isNeededItem(name)) {
                    ProtoPath path = struct.getNeededItem(name);
                    System.out.print("    local i_" + path.paramName() + " = ");
                } else {
                    System.out.print("    ");
                }
                Debug.println(10, "-- Handling adding localElt with f_" + fieldPrefix +
                                  "_" + printName() + " protoName " +
                                  protoName);
                System.out.println("t_" + protoName + ":" +
                                   Util.getLuaAdder(getType(), endianType) +
                                   "(f_" + fieldPrefix + "_" + printName() +
                                   ", buffer(offset, " +
                                   Util.baseTypeLenString(getType()) + "))");
                System.out.println("    offset = offset + " +
                                   Util.baseTypeLenString(getType()));
            }
        }
    }

    public class TypeDef {
        String name;
        String type;
        Boolean isArray;  // If it's an array.
        String arraySize;

        public TypeDef(String nName, String nType) {
            name = nName;
            type = nType;
            isArray = false;
        }

        // Get the length of this typedef. Either just that of the type
        // or arraysize by type size.
        String getLength() {
            int typeLen = Util.baseTypeLen(type); // Could be a struct?

            if (typeLen == 0)
                return "Unknown";

            if (isArray)
                return Integer.toString(Integer.decode(arraySize) * typeLen);
            else
                return Integer.toString(typeLen);
        }

        // An array of base type is also a base type.
        Boolean isBaseType() {
            if (Util.isBaseType(type)) {
                return true;
            } else {
                Debug.print(10, "//Type \"" + type + "\" for \"" +
                                   name);
                if (isArray)
                   Debug.print(10, "[" + arraySize + "]");
                Debug.println(10, "\" is not a base type");
                Debug.println(10, "//isArray " + isArray);
                return false;
            }
        }

        // Doesn't need to check up the stack! However, we do not want
        // our local isBaseType check. It is too restrictive.
        Boolean checkMissing() {
            Debug.println(10, "//Checking for missing typeDef " +
                               name);
            if (!Util.isBaseType(type) &&
                structs.get(type) == null) {
                System.out.println("Typedef " + name + " has a type (" +
                                   type + ") that does not resolve to a " +
                                   "base type or a scruct!");
                return true;
            }
            return false;
        }

        // Generate dissector code for an entity that refers to a typeDef.
        // We know the type info but we do not know the container and the
        // name it would appear under so they are passed to us.
        //
        // Some array types are handled directly, otherwise as an array
        // etc.
        void printDissector(String tree, StructInfo container, String name) {
            name = Util.printName(name); // Convert to usable format
            if (isArray) {
                if (Util.isSimpleArrayType(type)) {
                    System.out.println("    proto_tree_add_item(" + tree +
                                       ", hf_" + container.name + "_" +
                                       name + ", tvb, offset, " + arraySize +
                                       ", ENC_NA);");
                } else {
                    System.out.println("//Some stuff for arrays here");
                }
            } else { // Not an array ...
                if (Util.isBaseType(type)) {
                    System.out.println("// some stuff for non-array typedef");
                } else { // Must be a struct
                    StructInfo struct = structs.get(type);
                    String tree_name = "tree_" + name;
			// Insert a tree for this
                    System.out.println("  " + tree_name + " = proto_tree_add_subtree(" +
                                       tree + ", tvb, offset, -1, " +
                                       "ett_" + struct.name + ", &pi, " +
                                       name + ");");
                    System.out.println("  dissect_" + struct.name + "(" +
                                       "tvb, pinfo, " + tree_name +
                                       ", offset, xxx);");
                }
            }
        }

        // Need to generate declaration etc for etts
    }

    Parser parser;

    Boolean semanticError = false;

    Boolean needEiArray = false;

    // Next defines our endianness.
    EndianType endianType = EndianType.ENDIAN_BIG;

    StructInfo rootStruct; // Does this exist?
	// We keep all the structs in this hash map
	Map<String, StructInfo> structs =
		new LinkedHashMap<String, StructInfo>();

    // A hash map of all the enums we have seen
    Map<String, EnumInfo> enums = new LinkedHashMap<String, EnumInfo>();

    // A hasn map of all the typedefs we have seen
    Map<String, TypeDef> typedefs = new LinkedHashMap<String, TypeDef>();

    // The Dissector tables we have been told about
    // We need to relate the dissector etc.
    Map<String, String> dissectorTables =
		new LinkedHashMap<String, String>();

    Map<String, ProtoDetails> protoDetails =
		new LinkedHashMap<String, ProtoDetails>();

	// All of the top-level structs are kept here ...
	ArrayList<StructInfo> entryStructs = new ArrayList<StructInfo>();

    StructInfo tmpStruct;   // Used while assembling a struct
    EnumInfo tmpEnum;       // Used while assembling an enum
    ArrayList<Elt> tmpElts; // Used while assembling a struct

    // Set the parser so we can use it.
    void setParser(Parser parser) {
        this.parser = parser;
    }

    // Check for missing or invalid declarations
    // Loop through all the structs checking for problems
    Boolean missingDeclarations() {
        Boolean missingDecls = false;

        // For each struct defined, loop through
        for (StructInfo struct:entryStructs) {
            ArrayList<StructInfo> structStack = new ArrayList<StructInfo>();

            // We build up the dynamic stack as we go.
            if (struct != null && struct.checkMissing(structStack)) {
                missingDecls = true;
            }
        }

        return missingDecls;
    }

    // Mark structs and enums as reachable. This is being done elsewhere
    // but should perhaps not  overload other functions.
    void markReachable() {

    }

    // Setup the list of fields needed as items by structures referred
    // to from other structures.
    void setupNeededItemReferences() {
        for (StructInfo struct:entryStructs) {
            ArrayList<ProtoPath> neededItems = new ArrayList<ProtoPath>();

            Debug.println(10, "//Setting up needed item refs for " +
                              struct.name);
            struct.setupNeededItemReferences(neededItems);
            Debug.println(10, "//Done setting up needed item refs for " +
                              struct.name);
            if (neededItems.size() > 0) {
                System.out.println("Unexpected item references at the top level!");
                for (ProtoPath path:neededItems) {
                    System.out.println("  Item" + path.printPath() +
                                       " still needed by struct " +
                                       struct.name + "!");
                }
                semanticError = true;
            }
        }
    }

    // Setup the list of fields needed by those structures referred to
    // from other structures.
    void setupNeededReferences() {
        for (StructInfo struct:entryStructs) {
            // This is an array of Strings of format "StructName: field"
            // that represents all the fields needed from below us.
            // At the top level there should be none! At the next level
            // down there might be some. Structs cannot reach out and
            // across to other top-level structs as that makes no sense.
            ArrayList<ProtoPath> needed = new ArrayList<ProtoPath>();

            Debug.println(10, "//Setting up needed refs for " +
                              struct.name);
            struct.setupNeededReferences(needed);
            Debug.println(10, "//Done setting up needed refs for " +
                              struct.name);

            if (needed.size() > 0) {
                System.out.println("Unexpected references at the top level!");
                for (ProtoPath path:needed) {
                    System.out.println("  " + path.printPath() +
                                       " still needed by struct " +
                                       struct.name + "!");
                }
                semanticError = true;
            }
        }
    }

    // Generate the biolerplate required at the beginning of a dissector.
    void genInitialBoilerplate() {
        System.out.println("#include \"config.h\"");
        System.out.println("#include <epan/packet.h>");
        System.out.println("#include <epan/expert.h>");

        System.out.println("");
    }

    // Generate one registration
    void generateOneRegistration(ProtoDetails proto) {
        String name = Util.printName(proto.shortName);
        System.out.println("void\nproto_register_" + name + "(void)");
        System.out.println("{");
        System.out.println("  static hf_register_info hf[] = {");

        StructInfo struct = structs.get(proto.dissector_entry);
        struct.generateHfArray(Util.printName(proto.shortName + "." +
                               struct.name));

        System.out.println("  };\n");
        System.out.println("  static gint *ett[] = {");
        struct.generateEttArray();
        System.out.println("  };\n");
        // We leave the whole generation of the EI array up to generateEiArray
        // because there may not be any EIs.
        struct.generateEiArray(proto.shortName, needEiArray);
        System.out.println("  proto_" + name +
                           " = proto_register_protocol(" +
                           proto.name +  ", " + proto.shortName + ", " +
                           proto.abbrev + ");\n");
        System.out.println("  proto_register_field_array(proto_" +
                           name + ", hf, array_length(hf));");
        System.out.println("  proto_register_subtree_array(ett, " +
                           "array_length(ett));");
        // Now, register the EI array if there is any
        struct.generateEiArrayRegister(proto.shortName, needEiArray);
        System.out.println("}\n");
    }

    // Generate the registration routine. Generate the hf array, the
    // ett array, and the EI array and register them.
    void generateRegistration() {
        for (String key:protoDetails.keySet()) {
            generateOneRegistration(protoDetails.get(key));
        }
    }

    // Generate one trailer ...
    void generateOneTrailer(ProtoDetails proto) {
        String name = Util.printName(proto.shortName);
        System.out.println("void\nproto_reg_handoff_" + name +
                           "(void)");
        System.out.println("{");
        System.out.println("  static dissector_handle_t " + name +
                           "_handle;");
        System.out.println("  " + name + "_handle = " +
                           "create_dissector_handle(" + "dissect_" +
                           proto.dissector_entry + ", proto_" + name +
                           ");");
        System.out.println("  dissector_add_" + proto.tableType + "(" +
                           proto.dissector_table + ", " +
                           Util.unquoteString(proto.dissector_index) + ", " +
                           name + "_handle);");
        System.out.println("}");
    }

    // Generate the trailer code
    void generateTrailer() {
        for (String key:protoDetails.keySet()) {
            generateOneTrailer(protoDetails.get(key));
        }
    }

    // Generate the fixed portions and walk the various structures we have
    // to generate code for them.
    void generateCode() {
        System.out.println("// C Dissector generated from " + inputFile);
        // Generate boilerplate
        genInitialBoilerplate();

        // Generate HF fields for protocols
        for (String key:protoDetails.keySet()) {
            System.out.println("static int proto_" +
                               Util.printName(protoDetails.get(key).shortName) +
                               " = -1;");
        }

        // Generate the hf declarations
        for (String key:structs.keySet()) {
            structs.get(key).generateHfDecl(null);
        }

        // Generate the expert info declarations
        for (String key:structs.keySet()) {
            Boolean needEi = structs.get(key).generateEiDecl();
            if (needEi)
                needEiArray = true;
        }

        // Generate the ett declarations
        for (String key:structs.keySet()) {
            structs.get(key).generateEttDecl();
        }

        // Generate enum definitions
        for (String key:enums.keySet()) {
            enums.get(key).generateEnumDecl();
        }

        // Generate forward declarations
        for (String key:structs.keySet()) {
            structs.get(key).generateForwDecl();
        }
        System.out.println("");

        // Generate dissectors
        for (String key:structs.keySet()) {
            structs.get(key).generateDissectDecl();
        }

        // Generate registration code
        generateRegistration();

        // Generate trailer code
        generateTrailer();

    }

	public void exitEndianDecl(WiresharkGeneratorParser.EndianDeclContext ctx) {

        if (ctx.E_BIG() != null) {
            endianType = EndianType.ENDIAN_BIG;
        } else {
            endianType = EndianType.ENDIAN_LITTLE;
        }
	}

    // Put the whole thing together. Here we will walk the info we have
    // saved and build whatever we need.
    public void exitProtocol(WiresharkGeneratorParser.ProtocolContext ctx) {

        for (String key:typedefs.keySet()) {
            TypeDef tmp = typedefs.get(key);

            Debug.print(10, "/* typedef: key: " + key + ":" + tmp.type +
                            " " + tmp.name);
            if (tmp.isArray) {
                Debug.print(10, "[" + tmp.arraySize + "]");
            }
            Debug.println(10, " */");
        }

        // List the structs we have
        for (String key:structs.keySet()) {
            StructInfo tmp = structs.get(key);
            tmp.print();
        }

        // Check if we have any missing declarations or other errors...
        // and then generate code if none
        if (!missingDeclarations() && !semanticError) {
            // Run through each of the structures still accessible and
            // ensure that each level knows what is needed below and
            // above it.
            setupNeededReferences();
            if (!semanticError)
                setupNeededItemReferences();

            if (!semanticError)
                generateCode();
            else
                System.out.println("Semantic. No code generated");
        } else {
            System.out.println("Missing declarations or other errors (" +
                               "missing: " + missingDeclarations() +
                               "). No code generated");
        }
	}

    public void exitDissectorTableDecl(WiresharkGeneratorParser.DissectorTableDeclContext ctx) {
        String tableName = ctx.STRING(0).getText();
        String tableIndex = ctx.STRING(1).getText();
        String dissectorName = ctx.ID().getText();
        ProtoDetails protoVal = protoDetails.get(dissectorName);
        if (protoVal != null) {
            protoVal.setDissectorTable(tableName);
            protoVal.setDissectorIndex(tableIndex);
        } else {
            // FIXME: Defer this check until later. Add one here.
            System.out.println("Unable to find ProtoDetails for " +
                               dissectorName + "");
            System.out.println("Protos found:");
            for (String key:protoDetails.keySet()) {
                protoVal = protoDetails.get(key);
                System.out.println("  key: " + key + " DN: " +
                                   protoVal.name +
                                   "DE: " + protoVal.dissector_entry +
                                   "TN: " + protoVal.dissector_table +
                                   "INDX: " + protoVal.dissector_index);
            }
            semanticError = true;
        }
    }

    public void exitProtoDetailsDecl(WiresharkGeneratorParser.ProtoDetailsDeclContext ctx) {
        String formalName = ctx.STRING(0).getText();
        String shortName = ctx.STRING(1).getText();
        String abbrev = ctx.STRING(2).getText();
        String key = shortName.substring(1, shortName.length() - 1);

        protoDetails.put(key, new ProtoDetails(formalName, shortName, abbrev));

    }

    public void exitDissectorEntryDecl(WiresharkGeneratorParser.DissectorEntryDeclContext ctx) {
        String dissector = ctx.ID(0).getText();
        String dissectorEntry = ctx.ID(1).getText();

        ProtoDetails details = protoDetails.get(dissector);
        if (details == null) {
            // FIXME: Defer this check until later.
            System.out.println("Unknown dissector " + dissector +
                               " for dissectorEntry");
            //throw new RuntimeException(new org.antlr.v4.runtime.InputMismatchException(parser));
        } else {
            details.setDissectorEntry(dissectorEntry);
            // Set the isTopLevel value for the dissectorEntry.
            StructInfo struct = structs.get(dissectorEntry);

            Debug.println(10, "//Setting toplevel for " + dissectorEntry);
            if (struct != null) {
                struct.isTopLevel = true;
                struct.protoDetails = details; // Need this later
            } else {
                // Hmmm there is a better way to do this!
                Debug.println(10, "//Toplevel struct does not exist for " +
                          dissectorEntry);
            }
        }

        // If this has not been defined yet ...
        entryStructs.add(structs.get(dissectorEntry));
    }

    // Handle individual enum elts. Create a new elt and add it to the
    // tmpEnum that is already ready for us.
    public void exitEnumEltDecl(WiresharkGeneratorParser.EnumEltDeclContext ctx) {
        // If the INT is NULL, then it must be a default, so the STRING is
        // of interest.
        if (ctx.INT() == null) {
            tmpEnum.hasDefault = true;
            tmpEnum.defaultString = ctx.STRING().getText();
        } else {
            tmpEnum.addEnumElt(ctx.INT().getText(), ctx.ID().getText(),
                               (ctx.STRING() != null) ?
                                                ctx.STRING().getText() :
                                                null);
            tmpEnum.updateMaxIdLen(ctx.ID().getText().length());
        }
    }

    // Set up things needed for handling an EnumDecl
    public void enterEnumDecl(WiresharkGeneratorParser.EnumDeclContext ctx){
        tmpEnum = new EnumInfo();
    }

    // Insert the enum info into our list of enums.
    public void exitEnumDecl(WiresharkGeneratorParser.EnumDeclContext ctx) {

        tmpEnum.name = ctx.ID(0).getText();
        if (ctx.ID().size() > 1) { // There must be a type there
            tmpEnum.type = ctx.ID(1).getText();
            // Now, parse the type. Should be one of those allowed or
            // b1-bNN?
            int bitLen = Util.getTypeBitLen(tmpEnum.type);
            Debug.println(10, "//Bit len: " + bitLen + " type: " +
                           tmpEnum.type);
            if (bitLen == -1) { // Is it b1 to bNN?
                if (tmpEnum.type.charAt(0) == 'b') {
                    bitLen = Integer.parseInt(tmpEnum.type.substring(1));
                } else {
                    // There is an error. We should throw an error
                    semanticError = true;
                }
            }
            tmpEnum.bitLen = bitLen;
        }
        enums.put(tmpEnum.name, tmpEnum);
        tmpEnum = null;  // Should not access this now

    }

    // The format is typedef <some-type> <name>([<size>])? so we need
    // to rearrange the first two and check for the third.
	public void exitTypeDef(WiresharkGeneratorParser.TypeDefContext ctx) {

        TypeDef tmp = new TypeDef(ctx.ID(1).getText(), ctx.ID(0).getText());

        if (ctx.INT() != null) { // It's an array
            tmp.isArray = true;
            tmp.arraySize = ctx.INT().getText();
        }

        typedefs.put(ctx.ID(1).getText(), tmp);
	}

    // Build a linked list of the elements in the path.
    public ProtoPath getProtoPath(WiresharkGeneratorParser.FieldPathContext path) {
        ProtoPath pathComp = null, tmpComp = null;

        for (WiresharkGeneratorParser.FieldContext fld:path.field()) {

            if (pathComp == null) {
                pathComp = new ProtoPath();
                tmpComp = pathComp;
                if (path.start != null) {
                    switch (path.start.getType()) {
                    case WiresharkGeneratorParser.ABS:
                        pathComp.pathType = PathType.PATH_ABS;
                        break;
                    case WiresharkGeneratorParser.UP_ONE:
                        pathComp.pathType = PathType.PATH_RELATIVE_PARENT;
                        break;
                    }
                } else {
                    pathComp.pathType = PathType.PATH_RELATIVE_CURRENT;
                }
            } else {
                tmpComp.next = new ProtoPath();
                tmpComp = pathComp.next;
                tmpComp.pathType = PathType.PATH_COMPONENT;
            }

            // Now get the ID or STRING text ...
            if (fld.ID() != null) {
                tmpComp.componentName = fld.ID().getText();
            } else {
                tmpComp.componentName = fld.STRING().getText();
            }
            Debug.println(10, "//pathComp: " + tmpComp.componentName + " " +
                               tmpComp.pathType);
        }

        return pathComp;
    }

    // Parse a local elt decl - Each field has a name, but the subdivided
    // fields have separate names (and there is no name for the total field
    // in that case.)
    public LocalElt parseLocalEltDecl(WiresharkGeneratorParser.LocalEltDeclContext ctx,
                                      String parentName, int index) {
        LocalElt local = new LocalElt();
        local.type = ctx.ID(0).getText();
        local.definingLine = ctx.ID(0).getSymbol().getLine();

        Debug.println(10, "//Line = " + local.definingLine);
        if (ctx.localEltDeclCont().size() > 0) { // We have subdivisions
            local.isSubdivided = true;

            local.subElts = new ArrayList<SubElt>();

            // Deal with the subdivisions
            for (WiresharkGeneratorParser.LocalEltDeclContContext tmp:ctx.localEltDeclCont()) {
                SubElt subElt = new SubElt();

                subElt.parentType = local.type;
                subElt.type = tmp.ID(0).getText();
                if (tmp.ID().size() > 1) {
                    subElt.name = tmp.ID(1).getText();
                } else {
                    subElt.name = tmp.STRING().getText();
                }

                subElt.start = Integer.parseInt(tmp.INT(0).getText());
                if (tmp.INT().size() > 1) {
                    subElt.end = Integer.parseInt(tmp.INT(1).getText());
                } else {
                    subElt.end = subElt.start;
                }

                local.subElts.add(subElt);
            }
        } else {
            if (ctx.STRING() != null) {
                local.name = ctx.STRING().getText();
            } else {
                local.name = ctx.ID(1).getText();
            }
            Debug.println(10, "//  name = " + local.name);
        }

	// Need a local name, so create it.
	local.localName = parentName + "_" + String.valueOf(index);
	Debug.println(10, "//  localName = " + local.localName);
        return local;
    }

    // Parse an array elt decl
    public ArrayElt parseArrayEltDecl(WiresharkGeneratorParser.ArrayEltDeclContext ctx) {
        ArrayElt array;

        if (ctx.ID().size() > 1) {
            array = new ArrayElt(ctx.ID(1).getText());
        } else {
            array = new ArrayElt(ctx.STRING().getText());
        }

        array.definingLine = ctx.start.getLine();

        array.type = ctx.ID(0).getText();

        // Now process the switchStructEltCtrl if there is one otherwise
        // its an INT
        if (ctx.switchStructEltCtrl() != null)
            array.arrayCntl = new Expression(ctx.switchStructEltCtrl());
        else
            array.arrayCntl = new Expression(ctx.INT().getText());

	return array;
    }

    // We have an array elt decl
    //public void exitArrayEltDecl(WiresharkGeneratorParser.ArrayEltDeclContext ctx) {
    //    tmpElts.add(parseArrayEltDecl(ctx));
    //}

    // Parse out the structEltDecls in a caseDecl
    void parseStructEltDecls(ArrayList<Elt> elts,
                             List<WiresharkGeneratorParser.StructEltDeclContext> structs,
                             StructInfo parent, String extraName) {
        for (WiresharkGeneratorParser.StructEltDeclContext structElt:structs) {
            // We have to deal with externalEltDecl
            if (structElt.localEltDecl() != null) {
                Debug.println(10, "// Another local elt");
                elts.add(parseLocalEltDecl(structElt.localEltDecl(),
                                           parent.name +
                                               (extraName == null ?
                                                   "" : "_" + extraName),
                                           elts.size()));
            } else if (structElt.arrayEltDecl() != null) {
                elts.add(parseArrayEltDecl(structElt.arrayEltDecl()));
            } else if (structElt.switchDecl() != null) {
                elts.add(parseSwitchDecl(structElt.switchDecl(), parent));
            }
        }
    }

    // Parse out a function ref
    void parseFunctionDecl(ArrayList<Elt> elts,
		           WiresharkGeneratorParser.FunctionContext func) {
        FunctionElt fn = new FunctionElt();

        fn.name = func.ID().getText();
        fn.definingLine = func.start.getLine();

        // For the moment there must be at least three params.
        // When we generalize this, it may be less.
        if (func.params().param().size() < 3) {
            System.out.println("Error line " + func.start.getLine() +
                               ": An exception must have at least " +
                               "three parameters!");
            semanticError = true;
        }
        // A list of IDs, INTs or STRINGs. What if there is a parse error?
        for (WiresharkGeneratorParser.ParamContext
             param:func.params().param()) {
            if (param.INT() != null) {
                fn.params.add(new Param(param.INT().getText()));
            } else if (param.STRING() != null) {
                fn.params.add(new Param(param.STRING().getText()));
            } else if (param.ID() != null) {
                fn.params.add(new Param(param.ID().getText()));
            } else if (param.fieldPath() != null) { // It's a field path so we need a ProtoPath
                fn.params.add(new Param(param.fieldPath()));
            }
        }

        elts.add(fn);
    }

    // Parse out a case elt, either normal or default
    void parseCaseDecl(UnionCaseDecl decl,
                       WiresharkGeneratorParser.CaseDeclDetailsContext caseElt,
                       StructInfo parent, String labelText) {
	// We check the void case first!
        if (caseElt.ID() == null && caseElt.function() == null &&
            caseElt.structEltDecl().size() == 0) { // It's a void
            decl.definingLine = caseElt.start.getLine();
            decl.isVoid = true;
        } else if (caseElt.ID() == null &&
                   caseElt.function() == null) { // One or more structEltDecls
            parseStructEltDecls(decl.elts, caseElt.structEltDecl(),
                                parent, labelText);	
            decl.definingLine = caseElt.start.getLine();
	} else if (caseElt.ID() == null) { // It's a function ref
            parseFunctionDecl(decl.elts, caseElt.function());
        } else { /* It's the ID cases */
            LocalElt local = new LocalElt();
            local.type = caseElt.ID().getText();  // ID
            local.name = local.type;              // Does this need qualifying?
            local.definingLine = caseElt.ID().getSymbol().getLine();
	    decl.elts.add(local);
        }
    }

    // Parse switch decl
    public UnionElt parseSwitchDecl(WiresharkGeneratorParser.SwitchDeclContext ctx,
                                    StructInfo parent) {
        UnionElt union = new UnionElt();
        union.parent = parent;

        union.type = EltType.ELT_UNION;
        union.definingLine = ctx.start.getLine();

        Debug.println(10, "//Switch line: " + union.definingLine);

        union.unionCntl = new Expression(ctx.switchStructEltCtrl());

        // Now add the case elts to the Union
        // Deal with default and non-default branches. Also, if there
        // are params, the ID should be 'exception', 'warning', 'debug'
        // etc. Encode that otherwise indicate a semantic error.
        for (WiresharkGeneratorParser.CaseDeclContext caseElt:ctx.caseDecl()) {
            UnionCaseDecl decl = new UnionCaseDecl();
	    String labelText = "";

            if (caseElt.caseLabel() == null) { // The default
                decl.isDefault = true;
		labelText = "default";
                decl.definingLine = caseElt.start.getLine();
            } else if (caseElt.caseLabel().ID() != null) {
                decl.caseValue = caseElt.caseLabel().ID().getText();
		labelText = decl.caseValue;
                decl.definingLine = caseElt.caseLabel().ID().getSymbol().getLine();
            } else {  // Must be INT
                decl.caseValue = caseElt.caseLabel().INT().getText();
		labelText = decl.caseValue;
                decl.definingLine = caseElt.caseLabel().INT().getSymbol().getLine();
            }

            // Could be void, an ID with params, and ID, or more
            parseCaseDecl(decl, caseElt.caseDeclDetails(),
                          parent, labelText);

            union.cases.add(decl);

            decl.localName = tmpStruct.name + "_" +
                String.valueOf(union.cases.indexOf(decl));
        }
        return union;
    }

    public void enterStructDecl(WiresharkGeneratorParser.StructDeclContext ctx) {

        tmpStruct = new StructInfo(ctx.ID().getText());
        tmpElts = new ArrayList<Elt>();
    }

    public void exitStructDecl(WiresharkGeneratorParser.StructDeclContext ctx) {

        tmpStruct.definingLine = ctx.ID().getSymbol().getLine();
        tmpStruct.elts = tmpElts;
        parseStructEltDecls(tmpStruct.elts, ctx.structEltDecl(),
                            tmpStruct, null);
	Debug.println(10, "//Listing the elts in struct " + tmpStruct.name);
	for (Elt tmpElt:tmpElts) {
             tmpElt.print();
        }
	Debug.println(10, "//End Listing the elts in struct " + tmpStruct.name);
        tmpElts = null;
        structs.put(tmpStruct.name, tmpStruct);
        tmpStruct = null;
    }

}
