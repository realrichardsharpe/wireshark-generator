/*
 * Generate a Wireshark dissector by listening to all the enter and exit points
 * in the grammar.
 *
 * Copyright 2017, 2018 Richard Sharpe <realrichardsharpe@gmail.com>
 */

import java.util.*;

// A path to a field in a struct. Multiple components because we may
// have to access a field within another field. The final component
// is the one we want to test a switch or array control with.
// Using the pathtype scheme only allows us to go up one level. That
// may prove insufficient at some time.
class ProtoPath {
    PathType pathType;
    String componentName;
    ProtoPath next;
    String type;
    String wsType;

    public ProtoPath() {
        pathType = PathType.PATH_RELATIVE_CURRENT;
    }

    ProtoPath(ProtoPath src) {
        pathType = src.pathType;
        componentName = src.componentName;
        next = null;
        if (src.type != null)
            type = src.type;
    }

    // Build a linked list of the elements in the path. This should
    // only be called once on the path.
    public ProtoPath(WiresharkGeneratorParser.FieldPathContext path) {
        ProtoPath pathComp = null, tmpComp = null;
        pathType = PathType.PATH_RELATIVE_CURRENT;

        if (path.startSym != null) {
            switch (path.startSym.getType()) {
            case WiresharkGeneratorParser.ABS:
                pathType = PathType.PATH_ABS;
                break;
            case WiresharkGeneratorParser.UP_ONE:
                pathType = PathType.PATH_RELATIVE_PARENT;
                break;
            }
        }

        // Now, deal with each field, but the first one is special
        for (WiresharkGeneratorParser.FieldContext fld:path.field()) {

            if (pathComp == null) {
                pathComp = this;
                tmpComp = pathComp;
            } else {
                tmpComp = new ProtoPath();
                pathComp.next = tmpComp;
                pathComp = tmpComp;  // Shift gaze
            }

            // Now get the ID or STRING text ...
            if (fld.ID() != null) {
                tmpComp.componentName = fld.ID().getText();
            } else {
                tmpComp.componentName = fld.STRING().getText();
            }
            Debug.println(10, "//pathComp: " +
                              tmpComp.componentName + " " +
                              tmpComp.pathType);
        }
    }

    // Is this one level, ie, really local. Not above and not below
    Boolean isOneLevel() {
        return (pathType != PathType.PATH_RELATIVE_PARENT &&
                next == null);
    }

    // Set our type for later uses
    void setType(String type) {
        this.type = type;
    }

    // Get the name of our path as a param. This is the component
    // names concatenated with underscore.
    String paramName() {
        ProtoPath comp = this;
        String name = null;

        while (comp.next != null) {
            if (name == null) {
                name = Util.printName(comp.componentName);
            } else {
                name = name + "_" + Util.printName(comp.componentName);
            }
            comp = comp.next;
        }

        if (name == null)
            return Util.printName(comp.componentName);
        else
            return name + "_" + Util.printName(comp.componentName);
    }

    // Get the name of our path as a ptr to a param.
    String ptrParamName() {
       return "ptr_" + this.paramName();
    }

    // Remove parent-relative flag
    void removeParentRelative() {
        pathType = PathType.PATH_RELATIVE_CURRENT;
    }

    // Make us parent relative.
    void makeParentRelative() {
        pathType = PathType.PATH_RELATIVE_PARENT;
    }

    // Duplicate us from this point
    ProtoPath duplicate() {
        ProtoPath path = new ProtoPath(this);
        ProtoPath tail = next, pathComp = path;

        while (tail != null) {
             pathComp.next = tail.duplicate();
             tail = tail.next;
             pathComp = pathComp.next;
        }

        return path;
    }

    // If the first component equal to the string name
    Boolean equalsFirst(String name) {
        return name.equals(componentName);
    }

    // Is the last component equal to the string name
    Boolean equalsLast(String name) {
        ProtoPath last = this;

        while (last.next != null)
            last = last.next;

        return name.equals(last.componentName);
    }

    // Is the path equal to the String name
    Boolean equals(String name) {
        if (next != null)
            return false;

        if (pathType != PathType.PATH_RELATIVE_CURRENT)
            return false;

        if (name.equals(componentName))
            return true;

        return false;
    }

    // Are we equal, component-wise to path
    Boolean equals(ProtoPath path) {
        if (path == null)
            return false;

        if (pathType != path.pathType)
            return false;

        // Now walk the chain
        ProtoPath tmp1 = this, tmp2 = path;
        while (tmp1 != null) {
            if (!tmp1.componentName.equals(tmp2.componentName))
                return false;

            tmp1 = tmp1.next;
            tmp2 = tmp2.next;

            // We know that tmp2 was not null at the start of the loop
            if (tmp1 != null  && tmp2 == null)
                return false;
        }

        // If any left over, we are not equal
        if (tmp2 != null)
            return false;

        return true;
    }

    // Add ourselves to list if not already present
    void addIfNotPresent(ArrayList<ProtoPath> list, String listName) {
        for (ProtoPath tmp:list) {
            if (tmp.equals(this)) {
                return;
            }
        }
        list.add(this);
    }

    // Return the raw type as stored in the last component
    String getType() {
        ProtoPath tmp = this;

        while (tmp.next != null)
            tmp = tmp.next;

        return tmp.type;
    }

    void setWsType(String type) {
        wsType = type;
    }

    // Return the wireshark type for a type we understand
    String getWsType() {
        return wsType;
    }

    public String printPath() {
        String pathString = "";
        ProtoPath tmp = next;

        if (pathType != null) {
            switch (pathType) {
            case PATH_ABS:
                pathString = "/";
                break;
            case PATH_RELATIVE_PARENT:
                pathString = "../";
                break;
            case PATH_RELATIVE_CURRENT:
                pathString = "PR:";
                break;
            case PATH_COMPONENT:
                pathString = "PC:";
                break;
            }
        }

        pathString += componentName;

        while (tmp != null) {
            pathString += "/" + tmp.componentName;
            tmp = tmp.next;
        }

        return pathString;
    }
}
